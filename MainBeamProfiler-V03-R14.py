#! Python 3

import wx
import wx.lib.buttons as buttons
import serial
import time
import threading
import pickle
import wx.lib.plot
import os, sys
import numpy as np
import DataProcessing as dp
import BeamProfiler as bp
import imgConv as ic
import math
from PIL import Image
from datetime import date
import csv



#Combine translation stage with data-acquisition
#translation stage is controlled by Arduino running Grbl
#data acquisition is controlled by Parallax-Propeller (DataSpider module)
#BeamProfiler V000 started from XYZ-scan-V013   -   6 Feb 2020-FD
#First mod: separate functions into files
#Module BeamProfiler contains all global variables used,
#along with functions specific to data acquisition from DataSpider / Propeller
#Module DataProcessing contains all functions specific to data processing
#such as subtracting bkg value, calculating centroid and second moment

#V03-R001 - modified menu bar, added status bar - FD-04jul20
#added info on status bar
#added serial number for each scan
#added Z-zeroing button

#V03-R002  -  FD-25oct20  -  adding time acquisition - WIP
#V3.4  -  FD-05nov20  -  adding Beam Parameters BP save function
#save in file BP.csv,
#appending the calculated beam parameters from successjve XY scans
#added bp.BPZ boolean variable for this
#also added tracking moving X & Y

#V3.5 -   adding loading data - not started yet   13nov20
#V3.6 -   keeping BPZ data live memory - not started yet   25nov20
#save BP data in file BP-YYYY-MM-DD.csv
#V3.7 -   better graph of BPZ data live memory
#V3.7b -   fixing save bug   FD-09dec20
#V3.8 -   fixed 2D analysis + BPsave   FD-12dec20
#V3.8B - fixed saving bug - FD-20dec20  +  added status msg w/ X0&Y0
#V3.9 - Adding reading / comparing data - FD-20dec20
#V3.10-D  -  fixed bug in graph() defining travel when scan==2D line 1782 + 1790  FD-08jan21
# -E  11jan21  try fix 2D image orientation
#V3.11  -  try implement XY plot from 2D
#3.12  -  fixing bugs from 3.11  -  seems to work FD-14feb21-2:36pm
#3.12-C   fixed few small things, remd-out print, resetProcess when saving  FD-17feb21
#3.13   Implemented XY-graph + 4S BW on 2D data  -  29jan22

#version 
v = 3.14


V = "V" + str(v)

if bp.v != v or dp.v != v :
    print ("modules are not correct version")

title = "X-Y-Z Scan - All values in mm and seconds - " + V 
xSize = 800         #H-size of window frame
ySize = 700         #V-size of window frame
btnSize = 40        #size of START button
panelWidth = 180    #width of left panel

bkgStyle = (wx.DEFAULT_FRAME_STYLE | wx.STAY_ON_TOP) & ~(wx.RESIZE_BORDER | wx.MAXIMIZE_BOX)

class MyFrame(wx.Frame):
    def __init__(self, parent, title, xSize, ySize, panelWidth):
        wx.Frame.__init__(self, parent, panelWidth, title=title, size=(xSize, ySize))

        ico = wx.Icon('OFSI.ico', wx.BITMAP_TYPE_ICO )
        self.SetIcon( ico )

        global plot, mf, bkgReceived, NewLine, AddLine
        mf = self
        bkgReceived = False
        NewLine = False
        AddLine = False

        #timer used for data-acquisition
        self.stepTimer = wx.Timer(self)        

        #setting up variable with default values
        bp.updateSteps()
        self.grblPort = bp.grblPort
        self.stepXY = bp.stepXY
        self.Xtravel = bp.Xtravel
        self.Ytravel = bp.Ytravel
        self.nstepX = bp.nstepX
        self.nstepY = bp.nstepY
        self.nsteptot = bp.nsteptot
        self.analogIn = bp.analogIn
        self.duration = bp.duration
        self.propPort = bp.propPort
        self.stepZ = bp.stepZ
        self.travelZ = bp.travelZ
        self.scan = bp.scan

        self.scanList = ['X','Y','XY','2D','XYZ']

        self.centroid = bp.centroid

        #print "default values: ", self.grblPort, self.stepXY, self.Xtravel,\
                        #self.Ytravel, self.stepZ, self.travelZ,\
                        #self.analogIn, self.duration, self.propPort


        #defining all event handlers
        def getComPort(event):
            self.grblPort = self.gcomPort.GetValue()
            print ("selected port: "), self.grblPort
            bp.grblPort = self.grblPort
            return self.grblPort

        def getStepXY(event):
            self.stepXY = float(self.stepSize.GetValue())
            print ("step: "), self.stepXY
            bp.stepXY = self.stepXY
            bp.updateSteps()
            graph()
            self.updateStat('',1)
            return self.stepXY

        def getStepZ(event):
            self.stepZ = float(self.stepZSize.GetValue())
            print ("stepZ: "), self.stepZ
            bp.stepZ = self.stepZ
            self.updateStat('',1)
            return self.stepZ

        def getXTravel(event):
            global mf
            self.Xtravel = float(self.totalTravel.GetValue())
            print ("Xtravel: "), self.Xtravel
            bp.Xtravel = self.Xtravel
            bp.updateSteps()
            graph()
            self.updateStat('',1)
            return self.Xtravel
    
        #added 12mar20/FD
        def getYtravel(event):
            global mf
            self.Ytravel = float(self.totalYtravel.GetValue())
            print ("Ytravel: "), self.Ytravel
            bp.Ytravel = self.Ytravel
            bp.updateSteps()
            graph()
            self.updateStat('',1)
            return self.Ytravel
    
        def getPropPort(event):
            self.propPort = self.propComPort.GetValue()
            print ("selected prop port: "), self.propPort
            bp.propPort = self.propPort
            return self.propPort

        def getChanIn(event):
            self.analogIn = self.chanIn.GetValue()
            print ("selected AI-"),self.analogIn
            bp.analogIn = self.analogIn
            return self.analogIn

        def getDuration(event):
            self.duration = float(self.durAcq.GetValue())
            print ("duration: "), self.duration
            bp.duration = self.duration
            self.updateStat('',1)
            return self.duration

        def getBillboard(event):
            #time.sleep(1)
            billboard = BillboardDisplay(self)
            billboard.Show()

        def getGraph(event):
            #time.sleep(1)
            tgraph = TimeGraph(self)
            tgraph.Show()

        def moveLeft(event):
            global mf
            mf.statusbar.PushStatusText('moving left')
            grblPort = bp.grblPort
            stepXY = bp.stepXY
            try:
                if bp.s is None:
                    startArduino(grblPort,bp.grblbdr)
                else:
                    if not bp.s.isOpen():
                        bp.s.open()
            except:
                startArduino(grblPort,bp.grblbdr)
            gcode = 'G01 X-'  + str(stepXY)
            sendCode(gcode)
            bp.travelX = bp.travelX - stepXY
            print ('Xoffset: '),bp.travelX
            time.sleep(1)
            mf.statusbar.PushStatusText('  ')
            self.updateStat('',1)

        def moveRight(event):
            global mf
            self.updateStat('moving right',0)
            grblPort = bp.grblPort
            stepXY = bp.stepXY
            try:
                if bp.s is None:
                    startArduino(grblPort,bp.grblbdr)
                else:
                    if not bp.s.isOpen():
                        bp.s.open()
            except:
                startArduino(grblPort,bp.grblbdr)
            gcode = 'G01 X'  + str(stepXY)
            sendCode(gcode)
            bp.travelX = bp.travelX + stepXY
            print ('Xoffset: '),bp.travelX
            time.sleep(1)
            self.updateStat(' ',0)
            self.updateStat('',1)

        def moveDown(event):
            #global s
            self.updateStat('moving down',0)
            grblPort = bp.grblPort
            stepXY = bp.stepXY
            try:
                if bp.s is None:
                    startArduino(grblPort,bp.grblbdr)
                else:
                    if not bp.s.isOpen():
                        bp.s.open()
            except:
                startArduino(grblPort,bp.grblbdr)
            gcode = 'G01 Y-'  + str(stepXY)
            sendCode(gcode)
            time.sleep(0.5)
            bp.travelY = bp.travelY - stepXY
            print ('Yoffset: '),bp.travelY
            self.updateStat('     ',0)
            self.updateStat('',1)

        def moveUp(event):
            #global s
            self.updateStat('moving up',0)
            grblPort = bp.grblPort
            stepXY = bp.stepXY
            try:
                if bp.s is None:
                    startArduino(grblPort,bp.grblbdr)
                else:
                    if not bp.s.isOpen():
                        bp.s.open()
            except:
                startArduino(grblPort,bp.grblbdr)
            gcode = 'G01 Y'  + str(stepXY)
            sendCode(gcode)
            bp.travelY = bp.travelY + stepXY
            print ('Yoffset: '),bp.travelY
            time.sleep(0.5)
            self.updateStat('     ',0)
            self.updateStat('',1)

        def moveBack(event):
            #global s
            self.updateStat('moving back',0)
            grblPort = bp.grblPort
            stepZ = bp.stepZ
            travelZ = bp.travelZ
            try:
                if bp.s is None:
                    startArduino(grblPort,bp.grblbdr)
                else:
                    if not bp.s.isOpen():
                        bp.s.open()
            except:
                startArduino(grblPort,bp.grblbdr)
            gcode = 'G01 Z-'  + str(stepZ)
            sendCode(gcode)
            travelZ = travelZ - stepZ
            bp.travelZ = travelZ
            self.horZPos.SetLabel(label = "Z=" + str(bp.travelZ))
            time.sleep(0.5)
            self.updateStat('     ',0)

        def moveFwd(event):
            #global s
            self.updateStat('moving forward',0)
            grblPort = bp.grblPort
            stepZ = bp.stepZ
            travelZ = bp.travelZ
            try:
                if bp.s is None:
                    startArduino(grblPort,bp.grblbdr)
                else:
                    if not bp.s.isOpen():
                        bp.s.open()
            except:
                startArduino(grblPort,bp.grblbdr)
            gcode = 'G01 Z'  + str(stepZ)
            sendCode(gcode)
            travelZ = travelZ + stepZ
            bp.travelZ = travelZ
            self.horZPos.SetLabel(label = "Z=" + str(bp.travelZ))
            time.sleep(0.5)
            self.updateStat('    ',0)

        def zeroZ(event):
            #global s
            grblPort = bp.grblPort
            try:
                if bp.s is None:
                    startArduino(grblPort,bp.grblbdr)
                else:
                    if not bp.s.isOpen():
                        bp.s.open()
            except:
                startArduino(grblPort,bp.grblbdr)
            gcode = 'G92 Z0'
            sendCode(gcode)
            travelZ = 0
            bp.travelZ = travelZ
            self.horZPos.SetLabel(label = "Z=" + str(bp.travelZ))

        def getScan(event):
            self.scan = self.scanType.GetValue()
            print ("selected scan: "), self.scan
            bp.scan = self.scan
            bp.updateSteps()
            graph()
            self.updateStat('',1)
            if self.scan == 'XYZ':
                #should open window with explanation how it will work
                header = "BP Data from MainBeamProfiler"
                saveBP(self,header)
                bp.BPZ = True
                self.scan = 'XY'
                bp.scan = self.scan
            return self.scan

        def getStart(event):
            self.value = self.pulseBtn.GetValue()
            if self.stepTimer.IsRunning():
                print ("TIMER RUNNING!!!!!!")
                self.stepTimer.Stop()
                print ("TIMER IS STOPPED")
            if self.value:
                if len(bp.data) != 0:
                    del bp.data[0:len(bp.data)]
                self.btnLabel.SetLabel('STOP')
                #imgData = startScan(self)
                self.updateStat('start scan',0)
                bp.sn = bp.sn + 1
                startScan(self)
            else:
                self.stopScan()
                #self.btnLabel.SetLabel('START')


        ###---end of event handlers



            

        ###---setting up all visual elements of GUI / frame / display / window
            
        ###--- seting up menu-bar
        # create the menubar
        menuBar = wx.MenuBar()
        # create a first menu
        firstMenu = wx.Menu()
        saveMenuItem = firstMenu.Append(100, "Save Scan","save all scan data in file")
        saveBPMenuItem = firstMenu.Append(101, "Save BP","save Beam Parameters in file")
        loadBPMenuItem = firstMenu.Append(102,'load BP','load BP data from file')
        loadScanMenuItem = firstMenu.Append(103,'load Scan','load scan data from file')
        # create a 2nd menu
        secondMenu = wx.Menu()
        bkgMenuItem = secondMenu.Append(200, "Background","subtract background")
        centerMenuItem = secondMenu.Append(201, "Center","Calculate centroid")
        widthMenuItem= secondMenu.Append(202, "Width","Calculate second-moment")
        allMenuItem= secondMenu.Append(203, "All","Calculate all parameters")
        # create a 3rd menu
        thirdMenu = wx.Menu()
        graphBPMenuItem = thirdMenu.Append(300, "Graph BP","Graph BPZ data")
        scaleMenuItem = thirdMenu.Append(301, "Line Scale","rescale added line")
        infoMenuItem = thirdMenu.Append(302, "About","info on this")
        # create a 4th menu
        fourthMenu = wx.Menu()
        originMenuItem = fourthMenu.Append(400, "X0&Y0","Reset X-Y origin")
        comMenuItem = fourthMenu.Append(401, "ComPorts","Reset Ports")
        scanNbMenuItem = fourthMenu.Append(402, "Scan#","Reset Scan Nb")
        # add menu to menubar
        menuBar.Append(firstMenu, "&File")
        menuBar.Append(secondMenu, "&Analysis")
        menuBar.Append(thirdMenu, "&More")
        menuBar.Append(fourthMenu, "&Reset")
        # bind actions
        self.Bind(wx.EVT_MENU, self.save, saveMenuItem)
        self.Bind(wx.EVT_MENU, self.sBP, saveBPMenuItem)
        self.Bind(wx.EVT_MENU, loadBP, loadBPMenuItem)
        self.Bind(wx.EVT_MENU, loadScan, loadScanMenuItem)
        self.Bind(wx.EVT_MENU, self.bkgCor, bkgMenuItem)
        self.Bind(wx.EVT_MENU, self.calcCentroid, centerMenuItem)
        self.Bind(wx.EVT_MENU, self.calcWidth, widthMenuItem)
        self.Bind(wx.EVT_MENU, self.dataAnalysis, allMenuItem)
        self.Bind(wx.EVT_MENU, self.graphBP, graphBPMenuItem)
        self.Bind(wx.EVT_MENU, self.reScaleNewline, scaleMenuItem)
        self.Bind(wx.EVT_MENU, self.info, infoMenuItem)
        self.Bind(wx.EVT_MENU, self.rstX0Y0, originMenuItem)
        self.Bind(wx.EVT_MENU, self.rstComPorts, comMenuItem)
        self.Bind(wx.EVT_MENU, self.rstScanNb, scanNbMenuItem)
        # activate menubar
        self.SetMenuBar(menuBar)

        #creating a status bar with 2 fields to display messages
        self.statusbar = self.CreateStatusBar(3)
        self.statusbar.SetStatusWidths([panelWidth, -2,-1])
        #self.statusbar.SetStatusText('...')
        #self.statusbar.SetStatusText('...', 1)

        #setting up font size and type for labels
        font = wx.Font(12,wx.FONTFAMILY_DEFAULT, wx.FONTSTYLE_NORMAL, wx.FONTWEIGHT_NORMAL, False)
        font2 = wx.Font(11,wx.FONTFAMILY_DEFAULT, wx.FONTSTYLE_NORMAL, wx.FONTWEIGHT_BOLD, True)
        
        #setting up a vertically-split window
        self.splitter = wx.SplitterWindow(self, -1, style=wx.SP_LIVE_UPDATE)
        self.grblPanel = wx.Panel(self.splitter, -1)
        self.graphPanel = wx.Panel(self.splitter, -1)
        #self.graphPanel.SetBackgroundColour(wx.WHITE)

        self.splitter.SplitVertically(self.grblPanel, self.graphPanel,panelWidth)

        # add plot on graphPanel
        self.plotSizer = wx.BoxSizer(wx.VERTICAL)
        plot = wx.lib.plot.PlotCanvas(self.graphPanel, size=wx.Size(500,500), style=wx.EXPAND)
        plot.SetShowScrollbars(False)

        #setting up sizers for graphPanel
        self.plotSizer.Add(plot,1,wx.EXPAND,0)
        graphPnlSizr = wx.BoxSizer(wx.VERTICAL)
        graphPnlSizr.Add(self.plotSizer, 1, wx.EXPAND, 0)
        self.graphPanel.SetSizer(graphPnlSizr)

        # grbl-COM-Port combobox Control
        self.portList = ['COM3','COM4','COM5','COM6','COM7','COM8', 'COM9', 'COM10', 'COM11', 'COM12','COM13']
        self.gportLabel = wx.StaticText(self.grblPanel, label="Grbl Port")
        self.gcomPort = wx.ComboBox(self.grblPanel, choices=self.portList, value=self.grblPort, style=wx.CB_READONLY)
        self.Bind(wx.EVT_COMBOBOX, getComPort, self.gcomPort)
        s1 = wx.BoxSizer(wx.HORIZONTAL)
        s1.Add(self.gportLabel, 0, wx.ALL,2)
        s1.Add(self.gcomPort, 0, wx.ALL,2)

        #XY-step size entry box
        self.stepLabel = wx.StaticText(self.grblPanel, wx.ID_ANY, 'XY Stp ')
        self.stepSize = wx.TextCtrl(self.grblPanel, wx.ID_ANY, str(bp.stepXY), style=wx.TE_PROCESS_ENTER)
        self.stepSize.Bind(wx.EVT_TEXT_ENTER, getStepXY)
        s2 = wx.BoxSizer(wx.HORIZONTAL)
        s2.Add(self.stepLabel, 0, wx.ALL,2)
        s2.Add(self.stepSize, 1, wx.ALL,2)

        #total travelX entry box
        self.travelLabel = wx.StaticText(self.grblPanel, wx.ID_ANY, 'Xtravel')
        self.totalTravel = wx.TextCtrl(self.grblPanel, wx.ID_ANY,str(bp.Xtravel),style=wx.TE_PROCESS_ENTER)
        self.totalTravel.Bind(wx.EVT_TEXT_ENTER, getXTravel)
        s3 = wx.BoxSizer(wx.HORIZONTAL)
        s3.Add(self.travelLabel, 0, wx.ALL,2)
        s3.Add(self.totalTravel, 1, wx.ALL,2)
        s3.SetDimension(-1,-1,90,-1)
        
        #total travelY entry box --- added FD/12mar20
        self.YtravelLabel = wx.StaticText(self.grblPanel, wx.ID_ANY, 'Ytravel')
        self.totalYtravel = wx.TextCtrl(self.grblPanel, wx.ID_ANY,str(bp.Ytravel),style=wx.TE_PROCESS_ENTER)
        self.totalYtravel.Bind(wx.EVT_TEXT_ENTER, getYtravel)
        s4 = wx.BoxSizer(wx.HORIZONTAL)
        s4.Add(self.YtravelLabel, 0, wx.ALL,2)
        s4.Add(self.totalYtravel, 1, wx.ALL,2)
        s4.SetDimension(-1,-1,90,-1)
        
        #Z-step size entry box
        self.stepZLabel = wx.StaticText(self.grblPanel, wx.ID_ANY, 'Z Stp ')
        self.stepZSize = wx.TextCtrl(self.grblPanel, wx.ID_ANY, str(bp.stepZ), style=wx.TE_PROCESS_ENTER)
        self.stepZSize.Bind(wx.EVT_TEXT_ENTER, getStepZ)
        s5 = wx.BoxSizer(wx.HORIZONTAL)
        s5.Add(self.stepZLabel, 0, wx.ALL,2)
        s5.Add(self.stepZSize, 1, wx.ALL,2)

        #setting up manual movement of stage
        self.moveStageBox = wx.StaticBox(self.grblPanel, -1, 'Move Stage')
        self.moveStageBox.SetFont(font)
        s20 = wx.StaticBoxSizer(self.moveStageBox, wx.VERTICAL)
        self.stageNote = wx.StaticText(self.grblPanel, label = '(increments of Step Size)')
        stageNoteSizer = wx.BoxSizer(wx.VERTICAL)
        stageNoteSizer.Add(self.stageNote, 0, wx.ALIGN_CENTER)
        stageNoteSizer.AddSpacer(2)
        s20.Add(stageNoteSizer, 0, wx.ALIGN_LEFT, 0)

        #Added labels X and Y, and buttons Down and Up - FD 31may18
        self.horizontalNote = wx.StaticText(self.grblPanel, label = "Horizontal X")
        horizontalNoteSizer = wx.BoxSizer(wx.VERTICAL)
        horizontalNoteSizer.Add(self.horizontalNote, 0, wx.ALIGN_CENTER)
        s20.Add(horizontalNoteSizer, 0, wx.ALIGN_CENTER, 0)
        
        self.moveLeftBtn = wx.Button(self.grblPanel, id=wx.ID_ANY, label = " - <<< ", size = (70,-1))
        self.moveRightBtn = wx.Button(self.grblPanel, id=wx.ID_ANY, label = " >>> + ", size = (70,-1))
        self.moveLeftBtn.Bind(wx.EVT_BUTTON, moveLeft)
        self.moveRightBtn.Bind(wx.EVT_BUTTON, moveRight)
        moveBtnSizer = wx.BoxSizer(wx.HORIZONTAL)
        moveBtnSizer.Add(self.moveLeftBtn,0,wx.CENTER)
        moveBtnSizer.Add(self.moveRightBtn, 0, wx.CENTER)
        s20.Add(moveBtnSizer, 0, wx.EXPAND, 0)

        self.verticalNote = wx.StaticText(self.grblPanel, label = "Vertical Y")
        verticalNoteSizer = wx.BoxSizer(wx.VERTICAL)
        verticalNoteSizer.Add(self.verticalNote, 0, wx.ALIGN_CENTER)
        s20.Add(verticalNoteSizer, 0, wx.ALIGN_CENTER, 0)
        
        self.moveDownBtn = wx.Button(self.grblPanel, id=wx.ID_ANY, label = "- \/ \/", size = (70,-1))
        self.moveUpBtn = wx.Button(self.grblPanel, id=wx.ID_ANY, label = " /\ /\ +", size = (70,-1))
        self.moveDownBtn.Bind(wx.EVT_BUTTON, moveDown)
        self.moveUpBtn.Bind(wx.EVT_BUTTON, moveUp)
        moveYBtnSizer = wx.BoxSizer(wx.HORIZONTAL)
        moveYBtnSizer.Add(self.moveDownBtn,0,wx.CENTER)
        moveYBtnSizer.Add(self.moveUpBtn, 0, wx.CENTER)
        s20.Add(moveYBtnSizer, 0, wx.EXPAND, 0)

        #adding Z-controls 
        self.horizontalZNote = wx.StaticText(self.grblPanel, label = "Z-axis")
        horizontalZNoteSizer = wx.BoxSizer(wx.VERTICAL)
        horizontalZNoteSizer.Add(self.horizontalZNote, 0, wx.ALIGN_CENTER)
        s20.Add(horizontalZNoteSizer, 0, wx.ALIGN_CENTER, 0)
        
        self.moveBwdBtn = wx.Button(self.grblPanel, id=wx.ID_ANY, label = "- <<< ", size = (70,-1))
        self.moveFwdBtn = wx.Button(self.grblPanel, id=wx.ID_ANY, label = " >>> +", size = (70,-1))
        self.moveBwdBtn.Bind(wx.EVT_BUTTON, moveBack)
        self.moveFwdBtn.Bind(wx.EVT_BUTTON, moveFwd)
        moveZBtnSizer = wx.BoxSizer(wx.HORIZONTAL)
        moveZBtnSizer.Add(self.moveBwdBtn,0,wx.CENTER)
        moveZBtnSizer.Add(self.moveFwdBtn, 0, wx.CENTER)
        s20.Add(moveZBtnSizer, 0, wx.EXPAND, 0)

        #adding Z-position - 23 jan 20  - added Z-zero - 21 oct 20 
        self.horZPos = wx.StaticText(self.grblPanel, label = "Z=" + str(bp.dataInput[6]), size = (50,-1))
        self.horZPos.SetFont(font2)
        self.zeroZBtn = wx.Button(self.grblPanel, id=wx.ID_ANY, label = "Z0", size = (35,-1))
        self.zeroZBtn.Bind(wx.EVT_BUTTON, zeroZ)
        horZposSzr = wx.BoxSizer(wx.HORIZONTAL)
        horZposSzr.Add(self.horZPos, 0, wx.ALIGN_LEFT)
        horZposSzr.Add(self.zeroZBtn, 0, wx.RIGHT)
        s20.Add(horZposSzr, 0, wx.ALIGN_CENTER, 0)


        # CH-IN combobox Control
        self.chanList = ['0', '1', '2', '3']
        self.chanLabel = wx.StaticText(self.grblPanel, label="Analog Input")
        self.chanIn = wx.ComboBox(self.grblPanel, choices=self.chanList, value=self.analogIn, style=wx.CB_READONLY)
        self.Bind(wx.EVT_COMBOBOX, getChanIn, self.chanIn)
        s30 = wx.BoxSizer(wx.HORIZONTAL)
        s30.Add(self.chanLabel, 0, wx.ALL,2)
        s30.Add(self.chanIn, 0, wx.ALL,2)

        #acquisition duration entry box
        self.durationLabel = wx.StaticText(self.grblPanel, wx.ID_ANY, 'Duration')
        self.durAcq = wx.TextCtrl(self.grblPanel, wx.ID_ANY,str(self.duration),style=wx.TE_PROCESS_ENTER)
        self.durAcq.Bind(wx.EVT_TEXT_ENTER, getDuration)
        s31 = wx.BoxSizer(wx.HORIZONTAL)
        s31.Add(self.durationLabel, 0, wx.ALL,2)
        s31.Add(self.durAcq, 0, wx.ALL,2)

        # Prop COM-Port combobox Control
        self.propPortLabel = wx.StaticText(self.grblPanel, label="Prop Port")
        self.propComPort = wx.ComboBox(self.grblPanel, choices=self.portList, value=self.propPort, style=wx.CB_READONLY)
        self.Bind(wx.EVT_COMBOBOX, getPropPort, self.propComPort)
        s32 = wx.BoxSizer(wx.HORIZONTAL)
        s32.Add(self.propPortLabel, 0, wx.ALL,2)
        s32.Add(self.propComPort, 0, wx.ALL,2)

        #setting up "billboard" button
        self.billboardBox = wx.StaticBox(self.grblPanel, -1, 'Display Live Data')
        self.billboardBox.SetFont(font)
        s40 = wx.StaticBoxSizer(self.billboardBox, wx.HORIZONTAL)
        self.billboardBtn = wx.Button(self.grblPanel, id=wx.ID_ANY, label = "BILLBOARD", size = (75,-1))
        self.billboardBtn.Bind(wx.EVT_BUTTON, getBillboard)
        s40.Add(self.billboardBtn, 0, wx.ALIGN_LEFT, 0)

        #Adding time graph - 25oct20
        self.graphBtn = wx.Button(self.grblPanel, id=wx.ID_ANY, label = "t-graph", size = (75,-1))
        self.graphBtn.Bind(wx.EVT_BUTTON, getGraph)
        s40.Add(self.graphBtn, 0, wx.RIGHT, 0)
        

        #adding type of scan choice - 7 Feb 20
        self.scanLbl = wx.StaticText(self.grblPanel, label = "Scan Type")
        self.scanType = wx.ComboBox(self.grblPanel, choices=self.scanList, value=self.scan, style=wx.CB_READONLY)
        self.Bind(wx.EVT_COMBOBOX, getScan, self.scanType)
        s52 = wx.BoxSizer(wx.VERTICAL)
        s52.Add(self.scanLbl, 0, wx.ALIGN_CENTER_HORIZONTAL, 0)
        s52.Add(self.scanType, 0, wx.ALIGN_CENTER_HORIZONTAL, 0)
        
        #setting up START toggle button with custom bitmap
        self.pulseOn = scale_bitmap(wx.Bitmap("record-button-on.png"), btnSize, btnSize)
        self.pulseOff = scale_bitmap(wx.Bitmap("record-button-off.png"),btnSize, btnSize)
        self.btnLabel = wx.StaticText(self.grblPanel, label="START")
        self.pulseBtn = buttons.GenBitmapToggleButton(self.grblPanel, id=wx.ID_ANY, bitmap=self.pulseOff)
        self.pulseBtn.SetBitmapSelected(self.pulseOn)
        self.pulseBtn.Bind(wx.EVT_BUTTON, getStart)
        s51 = wx.BoxSizer(wx.VERTICAL)
        s51.Add(self.btnLabel, 0, wx.ALIGN_CENTER_HORIZONTAL,0)
        s51.Add(self.pulseBtn, 0, wx.ALIGN_CENTER_HORIZONTAL,0)

        #putting START btn + ScanChoice together
        s50 = wx.BoxSizer(wx.HORIZONTAL)
        s50.Add(s52,0, wx.CENTER,0)
        s50.AddSpacer(20)
        s50.Add(s51,0, wx.CENTER,0)

        #defining labels for grblPanel
        self.grblLabel = wx.StaticText(self.grblPanel, -1, 'XYZ Stage Controls')
        self.grblLabel.SetFont(font)
        self.controlLabel = wx.StaticText(self.grblPanel, -1, 'Data Controls')
        self.controlLabel.SetFont(font)
        
       #putting together all the elements of grblPanel
        grblPnlSizr = wx.BoxSizer(wx.VERTICAL)
        grblPnlSizr.Add(self.grblLabel, 0, wx.CENTER, wx.ALL,2)
        grblPnlSizr.Add(wx.StaticLine(self.grblPanel,), 0, wx.ALL|wx.EXPAND, 5)
        grblPnlSizr.Add(s1, 0, wx.CENTER)
        grblPnlSizr.Add(s2, 0, wx.CENTER)
        grblPnlSizr.Add(s3, 0, wx.CENTER)
        grblPnlSizr.Add(s4, 0, wx.CENTER)
        grblPnlSizr.Add(s5, 0, wx.CENTER)
        grblPnlSizr.Add(s20, 0, wx.CENTER)
        grblPnlSizr.AddSpacer(3)
        grblPnlSizr.Add(self.controlLabel, 0, wx.CENTER)
        grblPnlSizr.Add(wx.StaticLine(self.grblPanel,), 0, wx.ALL|wx.EXPAND, 5)
        grblPnlSizr.Add(s30, 0, wx.CENTER)
        grblPnlSizr.Add(s31, 0, wx.CENTER)
        grblPnlSizr.Add(s32, 0, wx.CENTER)
        grblPnlSizr.AddSpacer(3)
        grblPnlSizr.Add(s40, 0, wx.CENTER)
        grblPnlSizr.AddSpacer(5)
        grblPnlSizr.Add(s50, 0, wx.CENTER)

        self.grblPanel.SetSizer(grblPnlSizr)
        
        self.Layout()
        self.Bind( wx.EVT_CLOSE, self.OnClose )

        graph()

        self.updateStat("",1)

    #----------------------------------------------------------------------

    def updateStat(mf,msg,f):
        global NewLine, AddedLine
        position = 'position  X0='+str(bp.travelX)+'  Y0='+str(bp.travelY) \
                + '  Z=' + str(bp.travelZ)
        mf.statusbar.SetStatusText(position, 2)
        if msg == "":
            try:
                if NewLine or AddedLine:
                    sNb = bp.rdNb
                else:
                    sNb = bp.sn
            except NameError:
                sNb = bp.sn
            msg = 'scan' + str(bp.scan) + ' Nb: ' + str(sNb)\
                  + '  X=' + str(bp.Xtravel) + '  Y=' + str(bp.Ytravel) \
                  + '  dX=' + str(bp.travelX) + '  dY=' + str(bp.travelY) \
                  + '  stp=' + str(bp.stepXY) + '  dur=' + str(bp.duration)

        mf.statusbar.SetStatusText(msg, f)
        if f != 0:
            if bp.LoadedFile:
                mf.statusbar.SetStatusText(V+"-"+bp.loadedFile, 0)
            else:
                mf.statusbar.SetStatusText(V, 0)
        

    #this is used......
    def save(self,event):
        print ("saving data...")
        self.statusbar.SetStatusText('saving data...', 1)
        header = "Data from MainBeamProfiler"
        saveData(self,header)
        
    def sBP(self,event):
        print ("saving data...")
        if len(bp.BPZdata) != 0 or bp.centroid != [0,0]:
            print ("saving data...")
            self.statusbar.SetStatusText('saving BP data...', 1)
            saveBP(self,bp.header)
        else:
            print ("no BP data to save...")

    def reScaleNewline(self,event):
        global NewLine, AddLine, lineLoaded
        print ('rescaling new line')
        scale = 1
        datScaled = []
        dlgscale = wx.TextEntryDialog(self,"Enter scaling factor for added line",'ScaleNewLine')
        dlgscale.ShowModal()
        if dlgscale.ShowModal() == wx.ID_OK :
            newscale = dlgscale.GetValue()
            dlgscale.Destroy()
        scale = float(newscale)
        if bp.scan == 'X' or bp.scan == 'Y':
            rddata = dp.dataFromFile(bp.rd,bp.rdScan)
            for n in range(len(rddata)):
                newval = rddata[n][1] * scale
                newPoint = (rddata[n][0],newval)
                datScaled = append(newPoint)
            lineLoaded = wx.lib.plot.PolyLine(dafScaled, colour='GREY', width=2)
        elif bp.rdScan == 'XY':
            datax, datay = dp.dataFromFile(bp.rd,bp.rdScan)
            dx=[]
            dy=[]
            for ni in range(len(datax)):
                dx.append( (datax[ni][0],datax[ni][1]*scale) )
                dy.append( (datay[ni][0],datay[ni][1]*scale) )
            linex = wx.lib.plot.PolyLine(dx, colour='ORANGE', width=2)
            liney = wx.lib.plot.PolyLine(dy, colour='CYAN', width=2)
            lineLoaded = [linex] + [liney]
        AddLine = True
        graph()
        pass


    def info(self, event):
        """"""
        print ("selected About")
        
    def rstX0Y0(self, event):
        """"""
        print ("selected Reset X0&Y0")
        bp.travelX = 0
        bp.travelY = 0
        self.updateStat('    ',0)
       #set current position as home
        try:
            bp.s.write(" G92 X0 Y0 \r \n")
            bp.s.write(" G91 F1000 \r \n")
        except:
            print ('Arduino com s is not open')
        
    def rstComPorts(self, event):
        """"""
        print ("selected Reset Com Ports")
        try:
            bp.s.close()
            bp.prop.close()
            print ('closed s and prop')
        except:
            print ("couldnot close s or prop")
        
    def rstScanNb(self, event):
        """"""
        print ("selected Reset Scan Nb")
        bp.sn = 0
        self.updateStat('    ',0)
        
    def bkgCor(self, event):
        """"""
        #print "selected bkg"
        if bp.scan == "X" or bp.scan == "Y":
            bkg = dp.CalcBkg(bp.scan, bp.data)
        elif bp.scan == "XY" or bp.scan=="XYZ":
            bkg = dp.CalcBkg(bp.scan, bp.XYdata)
        elif bp.scan == "2D":
            #print "Bkg correction does not work on 2D scan data"
            pass
        else:
            print ("Error.......")
            pass

        print ("bkgCor from loaded 2D"),bp.xyAnalysis
        enterVal = EnterValBkg(self,bkg,bkgStyle)
        enterVal.Show()
        return
        
    def calcCentroid(self, event):
        """"""
        #print "selected centroid"
        if bp.scan == "2D":
            bp.centroid = dp.calc_centroid(bp.imgData)
        elif bp.scan == "X" or bp.scan == "Y":
            bp.centroid = dp.calc_centroid(bp.data)
        elif bp.scan == "XY" or bp.scan=="XYZ" :
            bp.centroid = dp.calc_centroid(bp.XYdata)
            print ("returned centroid"),bp.centroid
            
        if bp.Analysis:
            self.calcWidth(event)
        else:
            displayResults = DisplayResults(self)
            displayResults.Show()
        return
        
    def calcWidth(self, event):
        """"""
        #print "selected width"
        if bp.scan == "2D":
            bp.secMoment = dp.calc_secmom(bp.imgData)
        elif bp.scan == "X" or bp.scan == "Y":
            bp.secMoment = dp.calc_secmom(bp.data)
        elif bp.scan == "XY" :
            bp.secMoment = dp.calc_secmom(bp.XYdata)
        print ("Received secMom"),bp.secMoment
        bp.Analysis = False
        displayResults = DisplayResults(self)
        displayResults.Show()

        Zpos = bp.travelZ
        Xcenter = bp.centroid[0] + bp.travelX - (0.5 * bp.Xtravel)
        Ycenter = bp.centroid[1] + bp.travelY - (0.5 * bp.Ytravel)
        Xwidth = bp.secMoment[0]
        Ywidth = bp.secMoment[1]
        beamData = [Zpos, Xcenter,Ycenter,Xwidth,Ywidth]

        #apparently this raises a TypeError
        #bp.BPZdata.append[beamData]
        
        if bp.BPZ:
            saveBP(self,'')
        return
        
    def dataAnalysis(self, event):
        """"""
        global mf
        #print "selected analysis"
        #this works, but there should be a waiting to enter value of bkg
        bp.resetProcess()
        bp.Analysis = True
        self.bkgCor(event)
        return

    #open a new frame to graph BP data (not used yet - FD-27nov20)
    #check for data to graph - 30nov20
    def graphBP(self,event):
        global gd,rd,pg
        try:
            lrd = len(rd)
        except:
            print ('rd does not exist')
            if len(bp.BPZdata) > 4:
                rd = bp.BPZdata
            elif bp.centroid == [0,0]:
                print ('no data to graph')
                return

        if len(rd) == 0 and bp.centroid != [0,0]:
            Zpos = bp.travelZ
            Xcenter = bp.centroid[0] + bp.travelX - (0.5 * bp.Xtravel)
            Ycenter = bp.centroid[1] + bp.travelY - (0.5 * bp.Ytravel)
            Xwidth = bp.secMoment[0]
            Ywidth = bp.secMoment[1]
            rd = [(Zpos, Xcenter,Ycenter,Xwidth,Ywidth)]

        else:            
            self.dummy = []
            self.nz = len(rd)-3
        
            if self.nz > 1:
                pass
            elif self.nz == 1:
                print ('only one data point - no graph')
                pass
            else:
                print ('no data to plot')
                pass
        gd = GraphBPData(self)
        gd.Show()
    
        

    #receive value for background subtraction
    def receiveBkg(self, bkg):
        #global imgData, data, dataInput, centroid, secMoment
        global plot, mf, bkgReceived
        #print "received"
        bkgReceived = True
        bkg = float(message)
        #print "new value received: ", bkg

        bp.imgData = dp.subtractBkg(bp.imgData,bkg)
        #reconstructing data to be plotted
        bp.data = dp.rebuildData(bp.imgData, bp.dataInput)
        #updating graph.plot
        graph()

        displayResults = DisplayResults(self)
        displayResults.Show()
        if bp.Analysis:
            self.calcCentroid(event)
        return


    #end scan data acquisition
    def stopScan(self):
        if bp.stp < bp.nsteptot:
            self.statusbar.SetStatusText('!!! scan interrupted !!!', 1)
            #print ("!!!scan interrupted!!!")
            interruptScan(self)
        else:
            #endScan(self)
            self.btnLabel.SetLabel('START')
            self.pulseBtn.SetBitmapSelected(self.pulseOff)
            self.pulseBtn.SetValue(False)
        

    def OnClose(self,event):
        try:
            bp.s.close()
            bp.prop.close()
        except:
            print ("couldnot close s or prop")
        self.Destroy()

#******-------- End of main window frame ------------*********


        
#*****-----Additional frames: BillboardDisplay, ShowImage, DisplayResults, EnterValBkg---
#********************************************************************************

##### Frame to display data value only
class BillboardDisplay(wx.Frame):
    def __init__(self, window_parent ):
        #global iloop, inc
        
        self.chan = int(bp.analogIn)
        #iloop = 12
        #inc = True
        self.timerPeriod = 200

        wx.Frame.__init__(self, window_parent, wx.ID_ANY, "BillBoard - AI" + str(self.chan))
        ico = wx.Icon('OFSI.ico', wx.BITMAP_TYPE_ICO )
        self.SetIcon( ico )

        self.timer = wx.Timer(self)
    
        mainSizer = wx.BoxSizer( wx.VERTICAL )
        panelSizer = wx.BoxSizer( wx.HORIZONTAL )
        self.font = wx.Font(200, wx.FONTFAMILY_TELETYPE, wx.FONTSTYLE_NORMAL, wx.FONTWEIGHT_BOLD)
    
        panel = wx.Panel(self)
        initVal = "0000"
        self.txtValue = wx.StaticText(self, label=initVal)
        self.txtValue.SetFont(self.font)
        self.gaugeMeter = wx.Gauge( self, wx.ID_ANY, 4096, wx.DefaultPosition, \
                        wx.Size( 75,-1 ), wx.GA_VERTICAL|wx.GA_SMOOTH )
        panelSizer.Add(self.txtValue, 0, wx.ALL, 20)
        panelSizer.Add((0,0),1)
        panelSizer.Add(self.gaugeMeter, 0, wx.ALL|wx.EXPAND, 5)
        panel.SetSizer(panelSizer)
        mainSizer.Add(panel, 1, wx.ALL|wx.EXPAND)
        self.SetSizer(mainSizer)
        self.Fit()

        bp.startADC(bp.propPort,bp.propbdr,self.chan,bp.rateVal)
        
        self.Bind(wx.EVT_TIMER, self.updateValue, self.timer)

        #changed from EVT_MOUSE_EVENTS to EVT_MOUSEWHEEL - works fine - FD 04jun18
        #first wheel clik starts, second one stops
        self.Bind( wx.EVT_MOUSEWHEEL, self.startTimer )
        self.Bind( wx.EVT_CLOSE, self.OnClose )

    def startTimer(self,event):
        #print "starting timer....."
        if self.timer.IsRunning():
            self.timer.Stop()
        else:
            self.timer.Start(self.timerPeriod)

    #read ADC and update value on billboard and gauge
    def updateValue(self,event):
        duration = 0.05
        data = bp.readData(duration)
        self.label = "{0:04}".format( data )
        self.txtValue.SetLabel(self.label)
        self.gaugeMeter.SetValue(data)

    def OnClose(self, event):
        #chIdx = int(dataInput[3])
        if self.timer.IsRunning():
            self.timer.Stop()
            #print "stoppped timer "
        try:
            bp.stopADC(self.chan)
            #print "stoppped ADC Chan "
        except:
            print ("Can't close prop")
        self.Destroy()




##### Frame to graph data value in time
#this still needs to be RE-DONE!!!  05nov20
#seems to be working  FD-10nov20
class TimeGraph(wx.Frame):
    def __init__(self, window_parent ):
        
        self.chan = int(bp.analogIn)
        self.timerPeriod = 200
        self.autoScale = True
        
        wx.Frame.__init__(self, window_parent, wx.ID_ANY, "Time Graph - AI" + str(self.chan))
        ico = wx.Icon('OFSI.ico', wx.BITMAP_TYPE_ICO )
        self.SetIcon( ico )
        
        # add menubar
        self.menuBar = wx.MenuBar( 0 )
        self.fileMenu = wx.Menu()
        self.editMenu = wx.Menu()
        # create & append menu items
        self.SaveMenuItem = self.fileMenu.Append(100,"Save","Save plot data in file")
        self.CloseMenuItem = self.fileMenu.Append(101,"Close","Close Plot Window")
        self.ScaleMenuItem = self.editMenu.Append(200,"Scale Plot","Change X or Y scale of plot")
        self.SamplingMenuItem = self.editMenu.Append(201,"Sampling","Change sampling rate")
        self.menuBar.Append(self.fileMenu, "File")
        self.menuBar.Append(self.editMenu, "Edit")
        # bind items
        self.Bind( wx.EVT_MENU, self.OnClose, self.CloseMenuItem )
        self.Bind( wx.EVT_MENU, self.OnSave, self.SaveMenuItem )
        self.Bind( wx.EVT_MENU, self.OnScale, self.ScaleMenuItem )
        self.Bind( wx.EVT_MENU, self.OnSampling, self.SamplingMenuItem)
        self.SetMenuBar( self.menuBar)
        
        self.DS = False
        self.t0 = 0
        self.DT = 0
        self.tdat = [(0,0)]
        self.xlabel = "Time (sec)"
        self.ylabel = "Intensity"
        self.ymax = 100
        self.xmax = 2
        self.xRange = (0,self.xmax)
        self.yRange = (0,self.ymax)

        self.plotTitleNS = "Time Plot " + str(self.chan) + " (Not Saved)"
        self.plotTitleDS = "Time Plot " + str(self.chan) + " (Data Saved)"
        
        self.timer = wx.Timer(self)
        self.Bind(wx.EVT_TIMER, self.updateValue, self.timer)
    
        mainSizer = wx.BoxSizer( wx.VERTICAL )
        
        # add plot
        self.plot = wx.lib.plot.PlotCanvas(self, size=wx.Size(500,300), style=wx.EXPAND)
        self.plot.SetShowScrollbars(True)
        mainSizer.Add(self.plot, 1, wx.EXPAND, 0)
        self.SetSizer(mainSizer)
        self.Fit()

        bp.startADC(bp.propPort,bp.propbdr,self.chan,bp.rateVal)

        self.updateValue(event = None)

        #changed from EVT_MOUSE_EVENTS to EVT_MOUSEWHEEL - works fine - FD 04jun18
        #first wheel clik starts, second one stops
        self.Bind( wx.EVT_MOUSEWHEEL, self.startTimer )
        self.Bind( wx.EVT_CLOSE, self.OnClose )

    def startTimer(self,event):
        #print "starting timer....."
        self.autoScale = True
        if self.timer.IsRunning():
            self.timer.Stop()
            self.DT = time.time() - self.t0
            #print ("stop time: "),self.DT
        else:
            self.timer.Start(self.timerPeriod)
            if self.DS:
                self.DS = False
                self.tdat = []
                self.DT = 0
                self.xmax = 2
                self.xRange = ( 0 , self.xmax )
            if self.DT == 0:
                self.t0 = time.time()
            else:
                self.t0 = time.time() - self.DT

    #read ADC and update value on billboard and gauge
    def updateValue(self,event):
        duration = 0.05

        if self.autoScale:
            datval = bp.readData(duration)
            if datval > self.ymax:
                self.ymax = 1.2 * datval
                self.yRange = (0,self.ymax)
            
            dt = time.time() - self.t0
        
            if dt > self.xmax:
                self.xmax = 5 * self.xmax
                self.xRange = ( 0 , self.xmax )
            
            datpt = (dt , datval)
            print ("data point "),datpt
            print ('tmax: '),self.xmax
            self.tdat.append(datpt)

        else:
            pass

        
        line = wx.lib.plot.PolyLine(self.tdat, colour='red', width=2)

        if self.DS:
            self.plotTitle = self.plotTitleDS
        else:
            self.plotTitle = self.plotTitleNS
        
        pg = wx.lib.plot.PlotGraphics([line], self.plotTitle, self.xlabel, self.ylabel)
        self.plot.Draw(pg, self.xRange, self.yRange)


    def OnSampling(self,event):
        print ('Change sampling rate...')
        curRate = 1000 / self.timerPeriod
        print ('timer period: '),self.timerPeriod
        print ('current rate: '),curRate
        dlg = wx.TextEntryDialog(self,"Enter new rate ",value = str(curRate))
        dlg.ShowModal()
        if dlg.ShowModal() == wx.ID_OK :
            newRate = int(dlg.GetValue())
            self.timerPeriod = 1000 / newRate + 10
            print ('new timer period: '),self.timerPeriod
            print ('new rate: '),newRate
            if self.timer.IsRunning():
                self.timer.Stop()
                self.DT = time.time() - self.t0
                self.startTimer(event = None)
            pass
        dlg.Destroy()
        pass
        
    def OnScale(self, event):
        #re-scale axes of graph plot
        dlgscl = wx.TextEntryDialog(self,"Enter new value",'xmin,xmax,ymin,ymax')
        dlgscl.ShowModal()
        if dlgscl.ShowModal() == wx.ID_OK :
            newScale = dlgscl.GetValue()
            dlgscl.Destroy()
            self.autoScale = False
            ci = 0
            newVal = []
            s1 = ''
            s2 = ''
            s3 = ''
            s4 = ''
            try:
                for c in newScale:
                    if ci == 3 and c != ',':
                        s4 = s4 + c
                    elif ci == 3 and c == ',':
                        ci = 4
                    if ci == 2 and c != ',':
                        s3 = s3 + c
                    elif ci == 2 and c == ',':
                        ci = 3
                    if ci == 1 and c != ',':
                        s2 = s2 + c
                    elif ci == 1 and c == ',':
                        ci = 2
                    if ci == 0 and c != ',':
                        s1 = s1 + c
                    elif ci == 0 and c == ',':
                        ci = 1
                    pass
                self.xmax = float(s2)
                self.ymax = float(s4)
                self.xRange = (float(s1), float(s2))
                self.yRange = (float(s3), float(s4))
            except:
                print ('Error parsing string xmin,xmax,ymin,ymax')
                pass
        #self.updateGraph(self.tdat,xr, yr)
        self.updateValue(event = None)
        print ('updated plot with new scale')
        pass

    def OnClose(self, event):
        #chIdx = int(dataInput[3])
        if self.timer.IsRunning():
            self.timer.Stop()
            #print "stoppped timer "
        try:
            bp.stopADC(self.chan)
            #print "stoppped ADC Chan "
        except:
            print ("Can't close prop")
        self.Destroy()

    def OnSave(self,event):
        global mf
        writeData = self.tdat
        if len(writeData) > 0 :
            filetypes = "CSV files (*.csv)|*.csv|Text files (*.txt)|*.txt|All files|*"
            dlg = wx.FileDialog(frame,"Choose a file", style=wx.FD_SAVE | wx.FD_OVERWRITE_PROMPT, wildcard=filetypes)
            outFile = None
            
            if dlg.ShowModal()==wx.ID_OK:
                try:
                    filename=dlg.GetFilename()
                    dirname=dlg.GetDirectory()
                    fullPath = os.path.join(dirname, filename)
                    date = '"' + time.asctime() + '"'
                    title = "saving data"
                    
                    dlg2 = wx.TextEntryDialog(frame, 'Enter some text','Text Entry')
                    dlg2.SetValue("")
                    if dlg2.ShowModal() == wx.ID_OK:
                        comment = dlg2.GetValue()
                        print('You entered: %s\n' % comment)
                    dlg2.Destroy()

                    print ("finename fullPath: "), fullPath

                    outFile = open(fullPath, "w")
                
                    # write header info
                    outFile.write( date )
                    outFile.write( "\n" )

                    # write comment info
                    outFile.write( comment )
                    outFile.write( "\n" )

                    # write data info
                    outFile.write( "sec,val" + "\n"  )

                    #write data
                    nData = len(writeData)

                    print ("info..."), comment
                    print ("data:"),writeData
                    strfmt = ''
                    

                    for nn in range(nData):
                        strfmt = str(writeData[nn][0]) + ','
                        strfmt = strfmt + str(writeData[nn][1]) + "\n"
                        outFile.write(strfmt)
                        
                    outFile.close()
                    print ("finished writing data in file")

                
                except IOError as e:
                    mf.updateStat('IOError', 0)
                    print ("Error opening file"), e
                except ValueError as e:
                    mf.updateStat('ValueError', 0)
                    print ("Error writing file"), e
                except:
                    mf.updateStat('Error saving', 0)
                    print ("Error in saving data")
                    outFile.close()

        self.DS = True
        self.updateValue(event = None)
        pass


    


#New frame to graph BPZdata
#        works  -  FD-04dec20
class GraphBPData(wx.Frame):
    def __init__(self, window_parent ):
        
        wx.Frame.__init__(self, window_parent, wx.ID_ANY, "Graph BP Data" )
        ico = wx.Icon('OFSI.ico', wx.BITMAP_TYPE_ICO )
        self.SetIcon( ico )
        global rd,pg,gd
        gd = self
        
        self.dummy = []
        self.nz = len(rd)-3
        
        if self.nz > 1:
            pass
        elif self.nz == 1:
            print ('only one data point - no graph')
            pass
        else:
            print ('no data to plot')
            pass
        
        # add menubar
        self.menuBar = wx.MenuBar( 0 )
        
        self.fileMenu = wx.Menu()
        self.editMenu = wx.Menu()
        self.menuBar.Append(self.fileMenu, "File")
        self.menuBar.Append(self.editMenu, "Graph")
        
        # create menu items
        save = wx.MenuItem( self.fileMenu, wx.ID_ANY, "Save", "Saves the current graph", wx.ITEM_NORMAL )
        close = wx.MenuItem( self.fileMenu, wx.ID_ANY, "Close", "Closes the graph window", wx.ITEM_NORMAL )
        scale = wx.MenuItem( self.editMenu, wx.ID_ANY, "Scale", "Re-scale graph", wx.ITEM_NORMAL )
        update = wx.MenuItem( self.editMenu, wx.ID_ANY, "Update", "Update graph", wx.ITEM_NORMAL )

        # append menu items
        self.fileMenu.AppendItem(save)
        self.fileMenu.AppendItem(close)
        self.editMenu.AppendItem(scale)
        self.editMenu.AppendItem(update)

        # bind items
        self.Bind( wx.EVT_MENU, self.OnClose, id=close.GetId() )
        self.Bind( wx.EVT_MENU, self.OnSave, id=save.GetId() )
        self.Bind( wx.EVT_MENU, self.OnScale, id=scale.GetId() )
        self.Bind( wx.EVT_MENU, self.Update, id=update.GetId() )
        self.SetMenuBar( self.menuBar)
        
        self.DS = False
        self.t0 = 0
        self.DT = 0
        self.tdat = [(0,0)]
        self.xlabel = "Distance Z (mm)"
        self.ylabel = "Value"
        self.ymin = 0
        self.ymax = 5
        self.xmin = 0
        self.xmax = 5
        self.xRange = (self.xmin,self.xmax)
        self.yRange = (self.ymin,self.ymax)
        self.autoScale = True
        self.X0 = []
        self.Y0 = []
        self.XW = []
        self.YW = []

        self.plotTitle = "BPZ Plot"
        #self.plotTitleDS = "BPZ Plot  (Data Saved)"
        
        mainSizer = wx.BoxSizer( wx.VERTICAL )
        
        # add plot
        self.plot = wx.lib.plot.PlotCanvas(self, size=wx.Size(500,300), style=wx.EXPAND)
        self.plot.SetShowScrollbars(True)
        
        mainSizer.Add(self.plot, 1, wx.EXPAND, 0)
        self.SetSizer(mainSizer)
        self.Fit()

        lin = wx.lib.plot.PolyLine(self.dummy, colour='green', width=2, legend = '')
        line = [lin]

        if len(rd) > 3:
            for dd in range(len(rd) - 3):
                zval = float( rd[3+dd][0] )
                x0val = float( rd[3+dd][1] )
                y0val = float( rd[3+dd][2] )
                xWidth = float( rd[3+dd][3] )
                yWidth = float( rd[3+dd][4] )
                print ("data: "), rd[3+dd], "dd: ", dd

                maxVal = max(x0val,y0val,xWidth,yWidth)
                minVal = min(x0val,y0val,xWidth,yWidth)
                
                if maxVal > self.ymax:
                    self.ymax = 1.2 * maxVal
                    self.yRange = (self.ymin,self.ymax)
                if minVal < self.ymin:
                    self.ymin = 1.2 * minVal
                    self.yRange = (self.ymin,self.ymax)

                dt = dd
                
                if zval > self.xmax:
                    self.xmax = 1.2 * zval
                    self.xRange = ( self.xmin , self.xmax )
                if zval < self.xmin:
                    self.xmin = 1.2 * zval
                    self.xRange = ( self.xmin , self.xmax )
                
                x0pt = (zval , x0val)
                y0pt = (zval , y0val)
                xwpt = (zval , xWidth)
                ywpt = (zval , yWidth)
                self.X0.append(x0pt)
                self.Y0.append(y0pt)
                self.XW.append(xwpt)
                self.YW.append(ywpt)
                print ("data: "), self.X0, self.Y0, self.XW, self.YW
                
            #print ("plot data: "),self.tdat
            line1 = wx.lib.plot.PolyLine(self.X0, colour='red', width=2,legend='X0')
            line2 = wx.lib.plot.PolyLine(self.Y0, colour='blue', width=2,legend='Y0')
            line3 = wx.lib.plot.PolyLine(self.XW, colour='orange', width=2,legend='Xwidth')
            line4 = wx.lib.plot.PolyLine(self.YW, colour='navy', width=2,legend='Ywidth')
            line = line + [line1] + [line2] + [line3] + [line4]
        else:
            pass

        pg = wx.lib.plot.PlotGraphics( line , self.plotTitle, self.xlabel, self.ylabel)
        self.plot.enableLegend = True
        self.plot.Draw(pg, self.xRange, self.yRange)

    def updateGraph(self):
        global rd, pg
        print ('updating graph')
        self.plot.Draw(pg, self.xRange, self.yRange)
        pass
    
    def update(self):
        zval = bp.travelZ
        x0val = bp.centroid[0] + bp.travelX - (0.5 * bp.Xtravel)
        y0val = bp.centroid[1] + bp.travelY - (0.5 * bp.Ytravel)
        xWidth = bp.secMoment[0]
        yWidth = bp.secMoment[1]
        
        x0pt = (zval , x0val)
        y0pt = (zval , y0val)
        xwpt = (zval , xWidth)
        ywpt = (zval , yWidth)
        self.X0.append(x0pt)
        self.Y0.append(y0pt)
        self.XW.append(xwpt)
        self.YW.append(ywpt)
                
        line1 = wx.lib.plot.PolyLine(self.X0, colour='red', width=2,legend='X0')
        line2 = wx.lib.plot.PolyLine(self.Y0, colour='blue', width=2,legend='Y0')
        line3 = wx.lib.plot.PolyLine(self.XW, colour='orange', width=2,legend='Xwidth')
        line4 = wx.lib.plot.PolyLine(self.YW, colour='navy', width=2,legend='Ywidth')
        line = line + [line1] + [line2] + [line3] + [line4]
        pg = wx.lib.plot.PlotGraphics( line , self.plotTitle, self.xlabel, self.ylabel)
        self.plot.enableLegend = True
        self.plot.Draw(pg, self.xRange, self.yRange)

        

    def OnClose(self, event):
        self.Destroy()


    def OnSave(self, event):
        pass


    def OnScale(self, event):
        print ('Selected Scaling Graph Menu')
        dlg = wx.TextEntryDialog(self,"Enter new value",'0,100,0,100')
        dlg.ShowModal()
        if dlg.ShowModal() == wx.ID_OK :
            newScale = dlg.GetValue()
            print ('new scale input: '), newScale
            self.autoScale = False
            ci = 0
            newVal = []
            s1=''
            s2=''
            s3=''
            s4=''
            for c in newScale:
                if ci == 3 and c != ',':
                    s4 = s4 + c
                elif ci == 3 and c == ',':
                    ci == 4
                elif ci == 2 and c != ',':
                    s3 = s3 + c
                elif ci == 2 and c == ',':
                    ci = 3
                elif ci == 1 and c != ',':
                    s2 = s2 + c
                elif ci == 1 and c == ',':
                    ci = 2
                elif ci == 0 and c != ',':
                    s1 = s1 + c
                elif ci == 0 and c == ',':
                    ci = 1
                pass

            print ('string parsed'), s1, s2, s3, s4
            self.xRange = (float(s1),float(s2))
            self.yRange = (float(s3),float(s4))
            
        else:
            self.autoScale = True
            xr = self.xRange
            yr = self.Yrange
        
        self.updateGraph()
        pass




###----Frame to display image of 2Dscan---------------------------------------
###---------------------------------------------------------------------------
class showImage(wx.Frame):
    def __init__(self, title = "BP_IMG " + str(bp.sn)):
        wx.Frame.__init__(self, None , -1, title, size=(350,350))

        print('showing image')
        #print bp.LoadedFile,bp.Loaded2D
        #print('data imgData:')
        #print bp.imgData

        global mf, showImg

        showImg = self

        # add menubar
        self.menuBar = wx.MenuBar( 0 )
        self.fileMenu = wx.Menu()
        self.graphMenu = wx.Menu()
        self.menuBar.Append(self.fileMenu, "File")
        self.menuBar.Append(self.graphMenu, "Graph")
        # create menu items - added x-plot and y-plot  29jan22-FD
        close = wx.MenuItem( self.fileMenu, wx.ID_ANY, "Close", "Closes the graph window", wx.ITEM_NORMAL )
        xPlot = wx.MenuItem( self.graphMenu, wx.ID_ANY, "X-plot", "Graph X traces", wx.ITEM_NORMAL )
        yPlot = wx.MenuItem( self.graphMenu, wx.ID_ANY, "Y-plot", "Graph Y traces", wx.ITEM_NORMAL )
        xyPlot = wx.MenuItem( self.graphMenu, wx.ID_ANY, "XY-plot", "Graph X-Y traces", wx.ITEM_NORMAL )
        xyAnalysis = wx.MenuItem( self.graphMenu, wx.ID_ANY, "XY-Analysis", "Graph X-Y traces + Analysis", wx.ITEM_NORMAL )
        # append menu items
        self.fileMenu.AppendItem(close)
        self.graphMenu.AppendItem(xPlot)
        self.graphMenu.AppendItem(yPlot)
        self.graphMenu.AppendItem(xyPlot)
        self.graphMenu.AppendItem(xyAnalysis)
        # bind items
        self.Bind( wx.EVT_MENU, self.OnClose, id=close.GetId() )
        self.Bind( wx.EVT_MENU, self.OnxPlot, id=xPlot.GetId() )
        self.Bind( wx.EVT_MENU, self.OnyPlot, id=yPlot.GetId() )
        self.Bind( wx.EVT_MENU, self.OnxyPlot, id=xyPlot.GetId() )
        self.Bind( wx.EVT_MENU, self.OnxyAnalysis, id=xyAnalysis.GetId() )
        self.SetMenuBar( self.menuBar)

        if bp.LoadedFile:
            sm0 = "Scan # " + str(bp.rdNb) + "  Z = " + str(bp.rdZpos)
            sm1 = str(bp.rdXtravel) + "mm x" + str(bp.rdYtravel) + "mm" +\
              "  __   X0=" + str(bp.rdtravelX) + "  Y0=" + str(bp.rdtravelY)
            self.SetTitle(bp.loadedFile)
            pass
        else:
            sm0 = "Scan # " + str(bp.sn) + "  Z = " + str(bp.travelZ)
            sm1 = str(bp.Xtravel) + "mm x" + str(bp.Ytravel) + "mm" +\
              "  __   X0=" + str(bp.travelX) + "  Y0=" + str(bp.travelY)

        #creating a status bar with 2 fields to display messages
        self.statusbar = self.CreateStatusBar(2)
        self.statusbar.SetStatusWidths([120, -1])
        self.statusbar.SetStatusText(sm0 , 0)
        self.statusbar.SetStatusText(sm1 , 1)

        mydata = np.asarray(bp.imgData)
        mydata = np.transpose(mydata)
        #note 23jun20-img is transposed, vertical is X and horizontal is Y
        #w/ transposition, it now matches X & Y, but Y is inverted
        #w/ flip, it is correct orientation
        h = mydata.shape[0]
        w = mydata.shape[1]
        f = int( 400 / w )
        #subtract bkg
        bp.bkg = np.amin(mydata)
        mydata = mydata - bp.bkg
        #re-orient the image to match real space
        mydata = np.flip(mydata,0)
        #scale to 255 max
        #defining vmax as a float, all following values are also float (decimals)
        vmax = float(np.amax(mydata))
        scale = 1.00001
        if vmax > 255:
            scale = (vmax / 255.000) * 100
            mydata = ( mydata * 100 ) / scale
        elif vmax == 0:
            scale = 1
            mydata = ( mydata * scale )
        else:
            scale = (255.000 / vmax) * 100
            mydata = ( mydata * scale ) / 100
        mydata = np.floor(mydata)
        mydata = mydata.astype(int)
        npimg = Image.fromarray(mydata).resize((f*w,f*h),resample=3, box=None)
        databmp = ic.WxBitmapFromPilImage(npimg)
        panel = wx.Panel(self,-1)
        btn = wx.BitmapButton(panel,-1,databmp,pos=(0,0))
        btn.SetDefault()

    def OnClose(self, event):
        self.Destroy()

    def OnxPlot(self, event):
        bp.xyAnalysis = False
        graphXfrom2D()

    def OnyPlot(self, event):
        bp.xyAnalysis = False
        graphYfrom2D()

    def OnxyPlot(self, event):
        bp.xyAnalysis = False
        graphXYfrom2D()

    def OnxyAnalysis(self, event):
        bp.xyAnalysis = True
        print ('setting xyAnalysis'),bp.xyAnalysis
        graphXYfrom2D()
        enterbkg = EnterValBkg(showImg,0,bkgStyle)
        enterbkg.Show()

    def calcCenter(self):
        bp.scan = "XY"
        bp.centroid = dp.calc_centroid(bp.XYdata)
        print ("showImg calcCenter: "),bp.centroid
        bp.secMoment = dp.calc_secmom(bp.XYdata)
        bp.xyAnalysis = False
        displayResults = DisplayResults(self)
        displayResults.Show()
        pass



####----Frame to display results
class DisplayResults(wx.Frame):
    def __init__(self, parent):
        wx.Frame.__init__(self, parent, title="BP X0&FW4S_Z=" + str(bp.travelZ))

        global x0, y0
        if bp.LoadedFile:
            smr0 = "Scan # " + str(bp.rdNb) + "  Z = " + str(bp.rdZpos)
            smr1 = str(bp.rdXtravel) + "mm x" + str(bp.rdYtravel) + "mm" +\
              " | x0=" + str(x0) + "  y0=" + str(y0)
            self.SetTitle(bp.loadedFile)
            center = bp.centroid
            width = bp.secMoment
            pass
        else:
            smr0 = "Scan # " + str(bp.sn) + "  Z = " + str(bp.travelZ)
            smr1 = str(bp.Xtravel) + "mm x" + str(bp.Ytravel) + "mm" +\
              " | X0=" + str(bp.travelX) + "  Y0=" + str(bp.travelY)
            center = bp.centroid
            width = bp.secMoment

        #creating a status bar with 2 fields to display messages
        self.statusbar = self.CreateStatusBar(2)
        self.statusbar.SetStatusWidths([120, -1])
        self.statusbar.SetStatusText(smr0 , 0) 
        self.statusbar.SetStatusText(smr1 , 1)
        
        titleFont = wx.Font(20, wx.FONTFAMILY_TELETYPE, wx.FONTSTYLE_NORMAL, wx.FONTWEIGHT_BOLD)
        resultsFont = wx.Font(16, wx.FONTFAMILY_TELETYPE, wx.FONTSTYLE_NORMAL, wx.FONTWEIGHT_BOLD)
        
        panel = wx.Panel(self)

        Xc = int(center[0]*1000) / 1000

        # Creating widget Boxes to display results 
        xAxisBox = wx.StaticBox(panel,-1,'X-Axis')
        yAxisBox = wx.StaticBox(panel,-1,'Y-Axis')
        xAxisBox.SetFont(titleFont)
        yAxisBox.SetFont(titleFont)

        xCenter = wx.StaticText(panel, -1, 'X0: ' + str(round(center[0],3)))
        yCenter = wx.StaticText(panel, -1, 'Y0: ' + str(round(center[1],3)))
        xCenter.SetFont(resultsFont)
        yCenter.SetFont(resultsFont)

        xWidth = wx.StaticText(panel, -1, 'FWx: ' + str(round(width[0],3)))
        yWidth = wx.StaticText(panel, -1, 'FWy: ' + str(round(width[1],3)))
        xWidth.SetFont(resultsFont)
        yWidth.SetFont(resultsFont)

        # Starts of sizers section
        sizerX = wx.StaticBoxSizer(xAxisBox, wx.VERTICAL)
        sizerY = wx.StaticBoxSizer(yAxisBox, wx.VERTICAL)
        mainSizer = wx.BoxSizer(wx.HORIZONTAL)
        
        sizerX.Add(xCenter, 0, wx.LEFT, 10)
        sizerX.Add(xWidth, 0, wx.LEFT, 10)
        sizerY.Add(yCenter, 0, wx.LEFT, 10)
        sizerY.Add(yWidth, 0, wx.LEFT, 10)
        mainSizer.Add(sizerX,1,wx.EXPAND|wx.ALL,20)
        mainSizer.Add(sizerY,1,wx.EXPAND|wx.ALL,20)
        
        panel.SetSizer(mainSizer)
        
    def OnClose(self, event):
        self.Destroy()


####----Frame to enter bkg value
class EnterValBkg(wx.Frame):
    def __init__(self, parent, bkg, style):
        wx.Frame.__init__(self, parent,
                          title='Enter Background Value',
                          size = (220,130), style = style)
        
        global newBkg
        newBkg = bkg
        #print "default bkg value: ", bkg

        self.panel = wx.Panel(self)
        titleFont = wx.Font(20, wx.FONTFAMILY_TELETYPE, wx.FONTSTYLE_NORMAL, wx.FONTWEIGHT_BOLD)
        resultsFont = wx.Font(16, wx.FONTFAMILY_TELETYPE, wx.FONTSTYLE_NORMAL, wx.FONTWEIGHT_BOLD)

        #bkg value entry box
        self.bkgLabel = wx.StaticText(self.panel, wx.ID_ANY, 'Bkg Value')
        self.bkgVal = wx.TextCtrl(self.panel, wx.ID_ANY,str(bkg),style=wx.TE_PROCESS_ENTER)
        self.bkgVal.Bind(wx.EVT_TEXT_ENTER, self.getBkg)
        
        #confirm entry box with 2 buttons
        self.okBtn = wx.Button(self.panel,-1,"OK") 
        self.okBtn.Bind(wx.EVT_BUTTON, self.okClick) 
        self.cancelBtn = wx.Button(self.panel,-1,"Cancel") 
        self.cancelBtn.Bind(wx.EVT_BUTTON, self.cancelClick) 

        #sizer widget for entry box
        szr1 = wx.BoxSizer(wx.HORIZONTAL)
        szr1.Add(self.bkgLabel, 0, wx.ALL,5)
        szr1.Add(self.bkgVal, 0, wx.ALL,5)

        #sizer widget for confirm choice
        szr2 = wx.BoxSizer(wx.HORIZONTAL)
        szr2.Add(self.okBtn, 0, wx.ALL,5)
        szr2.Add(self.cancelBtn, 0, wx.ALL,5)
        
        #sizer widget for complete panel window
        mainSzr = wx.BoxSizer(wx.VERTICAL)
        mainSzr.Add(szr1,1,wx.EXPAND|wx.ALL,8)
        mainSzr.Add(szr2,1,wx.EXPAND|wx.ALL,8)
        
        self.panel.SetSizer(mainSzr)

# Function to capture the value
    def getBkg(self,event):
        global newBkg
        newBkg = int(self.bkgVal.GetValue())
        #print "New bkg value: ", newBkg
# Function to confirm the value
    def okClick(self,event):
        global newBkg, mf, showImg
        bp.bkg = newBkg
        print "Value Confirmed! ", bp.bkg
        print ("nkgCor from loaded 2D"), bp.xyAnalysis
        
        if bp.xyAnalysis:
            bp.XYdata = dp.subtractBkg("XY", bp.XYdata, bp.bkg)
            #reconstructing data to be plotted
            bp.data = dp.rebuildData(bp.XYdata, bp.stepXY)
            graph()
            self.Destroy()
            showImg.calcCenter()
            pass

        else:
            if bp.scan == "X" or bp.scan == "Y":
                bp.data = dp.subtractBkg(bp.scan, bp.data, bp.bkg)
            elif bp.scan == "XY" or bp.scan == "2D":
                bp.XYdata = dp.subtractBkg(bp.scan, bp.XYdata, bp.bkg)
                #reconstructing data to be plotted
                bp.data = dp.rebuildData(bp.XYdata, bp.stepXY)
            graph()
            if bp.Analysis:
                self.Destroy()
                mf.calcCentroid(event)
        self.Destroy()

# Function to cancel the value
    def cancelClick(self,event):
        self.Destroy()
   
    def OnClose(self, event):
        self.Destroy()


####----Frame to select option of loading data
class SelectOption(wx.Frame):
    def __init__(self, parent):
        wx.Frame.__init__(self, parent,
                          title='Option',
                          size = (250,150))
        panel = wx.Panel(self)
        btn0 = wx.Button(panel, label = "Erase current", pos = (50,10)) 
        btn1 = wx.Button(panel, label = "Graph with Current", pos = (50,40)) 
        #btn2 = wx.Button(panel, label = "New Data", pos = (50,70)) 
        btn0.Bind(wx.EVT_BUTTON, self.EraseCurrent)
        btn1.Bind(wx.EVT_BUTTON, self.GraphWithCurrent) 
        #btn2.Bind(wx.EVT_BUTTON, self.NewData) 
		
    def EraseCurrent(self, event):
        self.Destroy()
        readErase()
        print ('Erase and Replace') 
    def GraphWithCurrent(self, event):
        self.Destroy()
        readWith()
        print ('Graph With Current') 
    def NewData(self, event):
        self.Destroy()
        readNew()
        print ('New Data') 
    def OnClose(self, event):
        self.Destroy()
#*********************************

#****----- End of additional frames-----------------------
#************************************************************************
#************************************************************************


#xxxxxxxxxxxxxxxx
#xxxxxxxxxxxxxxxx
#xxxxxxxxxxxxxxxx
        
def graphXfrom2D():
    global mf,lineLoaded,NewLine,AddLine, showImg, datXY, x0, y0
    #open dlg to ask if user wants to plot X-Y from 2D
    #enter position of center X0 & Y0 to plot X & Y traces
    print ("graphing X from 2D")

    
def graphYfrom2D():
    global mf,lineLoaded,NewLine,AddLine, showImg, datXY, x0, y0
    #open dlg to ask if user wants to plot X-Y from 2D
    #enter position of center X0 & Y0 to plot X & Y traces
    print ("graphing Y from 2D"), bp.Loaded2D
    
    if not bp.Loaded2D:
        NewLine = True
        AddLine = False
    elif bp.Loaded2D:
        AddLine = True
        NewLine = False
        bp.dataSaved = True
        bp.stepXY = bp.rdStp
        bp.scan = bp.rdScan
        
    plotYdlg = wx.TextEntryDialog(mf,"Enter X position of trace ",'center')
    plotYdlg.ShowModal()
    
    if plotYdlg.ShowModal() == wx.ID_OK :
        YY = plotYdlg.GetValue()
        plotYdlg.Destroy()
        ci = 0
        newVal = []
        s1 = ''
        #s2 = ''
        try:
            for c in YY:
                if ci == 0 and c != ',':
                    s1 = s1 + c
                elif ci == 0 and c == ',':
                    ci = 1
                pass
            x0 = float(s1)
            y0 = 0
            print('entered values x0 y0:'),x0
            
            datax, datay, bp.XYdata = dp.buildXYfrom2D(bp.imgData,bp.rdStp,x0,0)
            
            #print ('built datax'),datax
            #print ('built datay'),datay
            
            bp.data = datay
            #bp.stp = 2 * (len(datay)-1)
            bp.stp = len(datay) - 1    #changed 29jan22-FD
            #bp.Xtravel = float( datax[len(datax) - 1][0] )
            bp.Ytravel = float( datay[len(datay) - 1][0] ) 
            mf.updateStat('',1)
            bp.dataSaved = True
            #bp.nstepX = len(datax) - 1
            bp.nstepY = len(datay) - 1
            bp.nsteptot = bp.stp - 1
            #print bp.Xtravel
            print bp.Ytravel
                
            if not bp.xyAnalysis or True:
                #linex = wx.lib.plot.PolyLine(datax, colour='RED', width=2)
                print ('data line'), datay
                liney = wx.lib.plot.PolyLine(datay, colour='BLUE', width=2)
                try:
                    lineLoaded =  lineLoaded  +  [ liney ]
                    print ('adding liney to lineLoaded')
                except:
                    lineLoaded = [liney]
                    print ('generated lineLoaded from liney')
                
                bp.scan = 'Y'
                print ('read data - go to graph'),bp.scan
                print [lineLoaded]
                graph()

        except:
            print Exception
            print ('Error parsing string x0,y0')
            xyerr = wx.MessageDialog(mf, message='error parsing enrty', caption='error parsing')
            xyerr.ShowModal()
            if xyerr.ShowModal() == wx.ID_OK:
                xyerr.Destroy()
            pass



def graphXYfrom2D():
    global mf,lineLoaded,NewLine,AddLine, showImg, datXY, x0, y0
    #open dlg to ask if user wants to plot X-Y from 2D
    #enter position of center X0 & Y0 to plot X & Y traces
    print ("graphing XY from 2D")
    
    if not bp.Loaded2D:
        NewLine = True
        AddLine = False
    elif bp.Loaded2D:
        AddLine = True
        NewLine = False
        bp.dataSaved = True
        bp.stepXY = bp.rdStp
        bp.scan = bp.rdScan
        
    plotXYdlg = wx.TextEntryDialog(mf,"Enter position of center X0,Y0 ",'center')
    plotXYdlg.ShowModal()
    if plotXYdlg.ShowModal() == wx.ID_OK :
        XY = plotXYdlg.GetValue()
        plotXYdlg.Destroy()
        ci = 0
        newVal = []
        s1 = ''
        s2 = ''
        try:
            for c in XY:
                if ci == 1 and c != ',':
                    s2 = s2 + c
                elif ci == 1 and c == ',':
                    ci = 2
                if ci == 0 and c != ',':
                    s1 = s1 + c
                elif ci == 0 and c == ',':
                    ci = 1
                pass
            x0 = float(s1)
            y0 = float(s2)
            #print('entered values x0 y0:'),x0,y0
            
            datax, datay, bp.XYdata = dp.buildXYfrom2D(bp.imgData,bp.rdStp,x0,y0)
            
            #print ('built datax'),datax
            #print ('built datay'),datay
            
            bp.data = datay
            #bp.stp = 2 * (len(datay)-1)
            bp.stp = len(datax) + len(datay) - 2    #changed 29jan22-FD
            bp.Xtravel = float( datax[len(datax) - 1][0] )
            bp.Ytravel = float( datay[len(datay) - 1][0] ) 
            mf.updateStat('',1)
            bp.dataSaved = True
            bp.nstepX = len(datax) - 1
            bp.nstepY = len(datay) - 1
            bp.nsteptot = bp.stp - 1
            #print bp.Xtravel
            #print bp.Ytravel
                
            if not bp.xyAnalysis or True:
                linex = wx.lib.plot.PolyLine(datax, colour='RED', width=2)
                liney = wx.lib.plot.PolyLine(datay, colour='BLUE', width=2)
                lineLoaded = [linex] + [liney]
                bp.scan = 'XY'
                print ('read data - go to graph'),bp.scan
                graph()
                #this works FD-15jan21
                #but cannot perform further analysis
                #would need to re-build imgData as XY for further analysis

        except:
            print Exception
            print ('Error parsing string x0,y0')
            xyerr = wx.MessageDialog(mf, message='error parsing enrty', caption='error parsing')
            xyerr.ShowModal()
            if xyerr.ShowModal() == wx.ID_OK:
                xyerr.Destroy()
            pass

        #performing BP-Analysis from loaded 2D image on XY traces
        if bp.xyAnalysis:
                #re-building imgData destroys the loaded data FD-16jan21
                bp.XYdata = []
                Xismax = False
                Yismax = False
                print ('resetting imgData for XY-Analysis')
                np = max(len(datax),len(datay))
                nc = min(len(datax),len(datay))
                if len(datax) > len(datay):
                    Xismax = True
                elif len(datay) > len(datax):
                    Yismax =True
                print ('checked length of datx and datay')
                print Xismax,Yismax
                print np,nc
                for ni in range(np):
                    print('building imgData'), ni,nc
                    if ni < nc:
                        bp.XYdata.append( [datax[ni][1],datay[ni][1]] )
                    elif Xismax:
                        print ('filling Y with 0')
                        bp.XYdata.append( [datax[ni][1],0] )
                    elif Yismax:
                        print ('filling X with 0')
                        bp.XYdata.append( [0,datay[ni][1]] )
                    print bp.XYdata
                pass


def analysisFrom2D():
    bkgCor2Dxy()
    pass

def bkgCor2Dxy():
    enterVal = EnterValBkg(self,bkg,bkgStyle)
    enterVal.Show()
    return





###-----  function to define graphing of data --------------
        #still needs to be updated
def graph():  #plot is the panel on which the PlotCanvas is defined
    global plot, mf, lineLoaded, NewLine, AddLine, rdImax, rdTravel, x0, y0
    print ('graph - xyAnalysis'),bp.xyAnalysis, bp.scan

    try:
        newLine = lineLoaded
        NewLine = True
        print ('lineLoaded valid exist'),[newLine]
    except:
        NewLine = False
        AddLine = False
        print ('no valid lineLoaded')
    #print ('does NewLine or AddLine exist')
    print ('new or add: '),NewLine,' ** ',AddLine
    data0 = []
    scan = bp.scan
    stp = bp.stp
    step = bp.stepXY
    nPoints = len(bp.data)
    nstepX = bp.nstepX
    nstepY = bp.nstepY
    nsteptot = bp.nsteptot
    maxY = 1

    #print ('graphing   scantype:'),scan,' npoints',nPoints,' stp',stp
    #print ('nstepX'), nstepX,' nsteptot',nsteptot

    if bp.dataSaved :
        ds = "(" + bp.saveFile + ")"
    else:
        ds = '(NOT saved)'
    

    if not NewLine and not AddLine:
        if scan == 'Y':
            travel = bp.Ytravel
        elif scan == 'X' or scan == '2D':
            travel = bp.Xtravel
        elif scan == 'XY':
            travel = max(bp.Xtravel,bp.Ytravel)

    else:
        print('New or Add Line * '), scan
        if scan == 'Y':
            travel = max(bp.Ytravel,rdTravel)
        elif scan == 'X' or scan == '2D':
            travel = max(bp.Xtravel,rdTravel)
        elif scan == 'XY':
            #print ('finding travel value')
            travel = max(bp.Xtravel,bp.Ytravel,rdTravel)
        
        
    #print ("*** stp: "), stp,  "*** nPoints: ", nPoints
    
    if scan == '2D':
        Ystp = bp.stp % nstepX
        #print "graph 2D Y-stp: ", Ystp

    #find the max value
    #print ('find max in bp.data')
    for n in range(nPoints):
        if bp.data[n][1] > maxY:
            maxY = bp.data[n][1] * 1.1
            bp.maxY = maxY

    if NewLine or AddLine:
        maxY = max(maxY,rdImax)
    print ('max='),maxY

    #defining plotting axis ranges    
    xRange = (0, travel)
    yRange = (bp.minVal, maxY )
    print ('x/y ranges'),xRange,' ',yRange
  
    #defining plot for Xscan
    if scan == "X" or scan == 'Y':
        if scan == 'X':
            xRange = (0,bp.Xtravel)
            plotTitle = "Plot X-axis " + ds
            #print ('graphing Xscan data bp.data')
        if scan == 'Y':
            print ('Yscan'), bp.Ytravel
            print ('bp.Loaded '), bp.Loaded2D
            xRange = (0,bp.Ytravel)
            plotTitle = "Plot Y-axis " + ds
            print ('graphing Yscan data bp.data')
        #print bp.data
        if not NewLine and not AddLine:
            line = wx.lib.plot.PolyLine(bp.data, colour='red', width=2)
        #line = [line] + [newLine]
        if NewLine:
            print ('graph new line')
            pg = wx.lib.plot.PlotGraphics(newLine, plotTitle, bp.xlabel, bp.ylabel)
            
        elif AddLine:
            print ('add line from loaded data')
            try:
                pg = wx.lib.plot.PlotGraphics(line+lineLoaded, plotTitle, bp.xlabel, bp.ylabel)
                print('line exists already, adding lineLoaded to line')
            except:
                print ('no line exists, creating pg from lineLoaded')
                print [lineLoaded], plotTitle, bp.xlabel, bp.ylabel
                pg = wx.lib.plot.PlotGraphics(lineLoaded, plotTitle, bp.xlabel, bp.ylabel)
                print ('created pg lineLoaded') 
                
        else:
            #print ('graph bp.data')
            pg = wx.lib.plot.PlotGraphics([line], plotTitle, bp.xlabel, bp.ylabel)
        NewLine = False
        AddLine = False
        print ('moving to plot')
   
    #defining plot for XYscan
    if scan == "XY" or scan=="XYZ" or bp.xyAnalysis:
        print ('defining lines graph-XY / stp='),stp,'*n:',nstepX
        print ('data '),bp.data
        if stp < nstepX + 2:
            plotTitle = "Plot X-axis " + ds
            line = wx.lib.plot.PolyLine(bp.data, colour='red', width=2)
            pg = wx.lib.plot.PlotGraphics([line], plotTitle, bp.xlabel, bp.ylabel)
        elif stp < nsteptot :
            plotTitle = "Plot Y-axis " + ds
            line = wx.lib.plot.PolyLine(bp.data, colour='blue', width=2)
            pg = wx.lib.plot.PlotGraphics([line], plotTitle, bp.xlabel, bp.ylabel)
        else:
            #print ('case n=nsteptot end of scan'),bp.XYdata
            for n in range(nstepX + 1):
                dv = bp.XYdata[n][0]
                #print ('XY graph finding max'),n,dv
                if dv > maxY:
                    maxY = dv * 1.1
                yRange = (bp.minVal, maxY )
                travel0 = step * n
                dataPoint0 = (travel0,dv)
                data0.append(dataPoint0)
            plotTitle = "Plot X(red) & Y(blue) " + ds
            line0 = wx.lib.plot.PolyLine(data0, colour='red', width=1)
            line = wx.lib.plot.PolyLine(bp.data, colour='blue', width=1)
            scanLine = [line0]+[line]
            if NewLine:
                print ('graph new line from loadedLine')
                pg = wx.lib.plot.PlotGraphics(newLine, plotTitle, bp.xlabel, bp.ylabel)
            elif AddLine:
                print ('add line to graph')
                #pg = wx.lib.plot.PlotGraphics(newLine+scanLine, plotTitle, bp.xlabel, bp.ylabel)
                pg = wx.lib.plot.PlotGraphics(newLine, plotTitle, bp.xlabel, bp.ylabel)
            else:
                print ('lines from end of scan')
                pg = wx.lib.plot.PlotGraphics(scanLine, plotTitle, bp.xlabel, bp.ylabel)
            NewLine = False
            AddLine = False

    print ('graphing object pg '), [pg]
            
    #defining plot for 2Dscan
    # is it possible to add all the X-scans lines???
    print ('scan: '), scan
    
    if scan == "2D":
        plotTitle = "Plot 2D X-axis 0/" + str(bp.nstepY + 1) + ds
        if stp < nstepX + 1:
            line = wx.lib.plot.PolyLine(bp.data, colour='red', width=2)
            pg = wx.lib.plot.PlotGraphics([line], plotTitle, bp.xlabel, bp.ylabel)
        elif stp < nsteptot + 1:
            ystp = int(math.floor(bp.stp / bp.nstepX))
            xstp = bp.stp % bp.nstepX 
            plotTitle = "Plot 2D X-axis " + str(ystp) + "/" + str(bp.nstepY + 1) + ds
            line = wx.lib.plot.PolyLine(bp.data, colour='blue', width=2)
            pg = wx.lib.plot.PlotGraphics([line], plotTitle, bp.xlabel, bp.ylabel)


    print "---graphing -- -- ",plotTitle
    print ('xRange , yRange '), xRange , '...' , yRange
    plot.Draw(pg, xRange, yRange)
    print ('plot successfull')

    if bp.LoadedFile:
        print ('updating status bar')
        print bp.scan, bp.rdNb, bp.Xtravel, bp.Ytravel
        print x0, y0, bp.stepXY, bp.duration
        msg = 'scan' + str(bp.scan) + ' Nb: ' + str(bp.rdNb)\
                  + '  X=' + str(bp.Xtravel) + '  Y=' + str(bp.Ytravel) \
                  + '  x0=' + str(x0) + '  y0=' + str(y0) \
                  + '  stp=' + str(bp.stepXY) + '  dur=' + str(bp.duration)
        print ('sucessfully updated '), msg
    else:
        print ('generating status message')
        msg = 'scan' + str(bp.scan) + ' Nb: ' + str(bp.sn)\
                  + '  X=' + str(bp.Xtravel) + '  Y=' + str(bp.Ytravel) \
                  + '  dX=' + str(bp.travelX) + '  dY=' + str(bp.travelY) \
                  + '  stp=' + str(bp.stepXY) + '  dur=' + str(bp.duration)
        print ('success create ')

    #mf.updateStat(msg,1)
    print ('updating....')
    mf.statusbar.SetStatusText(msg , 1)
    
###-----  End of graphing data function --------------
       





### --- Starting scan: moving stage and acquiring data
def startScan(self):
    global mf
    mf = self
    #print "startscan self mf:", mf
    #initializing all variables
    mf.statusbar.SetStatusText('starting scan...', 1)
    bp.updateSteps()
    bp.resetData()
    bp.dataSaved = False
    bp.LoadedFile = False
    
    if bp.BPZ:
        bp.scan = "XY"

    propPort = bp.propPort
    chIdx = int(bp.analogIn)
    grblPort = bp.grblPort
    travelStep = bp.stepXY
    duration = bp.duration
    scan = bp.scan
    nstepX = bp.nstepX
    nstepY = bp.nstepY
    nsteptot = bp.nsteptot
    Xtravel = bp.Xtravel
    Ytravel = bp.Ytravel
    stepXY = bp.stepXY
    nsteptot = bp.nsteptot
    
    
    #bp.travelX = 0
    #bp.travelY = 0
    
    minVal = 0
    maxVal = 1

    #check and validate all input data
    try:
        ii = 0
        for dat in [chIdx,grblPort,travelStep,Xtravel,Ytravel,\
                    duration,propPort,scan,nsteptot]:
            #print dat
            if dat == 0 and ii != 0:
                raise ValueError
            ii += 1
        if not Xtravel > stepXY:
            #print "error: travel NOT> step"
            raise ValueError
    except ValueError:
        print ("invalid datainput")
        mf.statusbar.SetStatusText('invalid data... end scan', 1)
        endScan(mf)

    #making sure that the Arduino-grbl is initialized
    try:
        if bp.s is None:
            #print "bp.s is none"
            startArduino(bp.grblPort, bp.grblbdr)
        else:
            #print "bp.s is NOT none"
            if not bp.s.isOpen():
                #print "bp.s is NOT open"
                bp.s.open()
    except:
        #print ("exception - starting Arduino...")
        #print "port: ", bp.grblPort, "DataRate dr: ", bp.grblbdr
        startArduino(bp.grblPort, bp.grblbdr)

    #set current position as home
    bp.s.write(" G92 X0 Y0 \r \n")
    bp.s.write(" G91 F1000 \r \n")

    #move to edge start of scan (YES - FD-9jan20)----
    #print "travelling to edge of Scan X"
    #print "totalTravel: ", totalTravel
    travelEdgeX = ((nstepX) * 0.5) * travelStep + 1
    travelEdgeY = ((nstepY) * 0.5) * travelStep + 1
    #print "travelEdge: ", travelEdgeX

    mf.updateStat('parameters valid',0)

    #print "travelXedge: ", travelXedge
    if scan == "X" or scan == "XY" or scan == "XYZ":
        bp.s.write(str('G01 X-' + str(travelEdgeX) + '\r \n'))
        bp.s.write(str('G01 F' + str(bp.travelSpeed) + ' X1 \r \n'))
        time.sleep((travelEdgeX * 1.2)/bp.travelSpeed + 1)
    elif scan == "Y":
        bp.s.write(str('G01 Y-' + str(travelEdgeY) + '\r \n'))
        bp.s.write(str('G01 F' + str(bp.travelSpeed) + ' Y1 \r \n'))
        time.sleep((travelEdgeY * 1.2)/bp.travelSpeed + 1)
        pass
    elif scan == "2D":
        bp.s.write(str('G01 X-' + str(travelEdgeX) + 'Y-' + str(travelEdgeY) + '\r \n'))
        bp.s.write(str('G01 F' + str(bp.travelSpeed) + 'X1 Y1 \r \n'))
        time.sleep(((travelEdgeX + travelEdgeY) * 1.2)/bp.travelSpeed + 1)
        pass
    else:
        print ("error")
        mf.updateStat('scan type error',1)
        pass
    #print "ready to start data acquisition"
    ###################***********************

    #starting the ADC channel on DataSpider / propcom
    bp.startADC(bp.propPort,bp.propbdr,chIdx,bp.rateVal)
    time.sleep(0.1)
    
    #read and delete first data to clear all buffers - added 20jan20
    datVal = bp.readData(duration)
    datVal = 0
    del bp.data[:]
    bp.stp = 0
    bp.scanning = True

    self.updateStat('taking data',0)

    #print "starting dataAcquisition...."
    #use a timer to initiate each data reading
    self.Bind(wx.EVT_TIMER, takeData, self.stepTimer)
    stepDelay = (0.1 + (stepXY*60/bp.travelSpeed))*1000
    stepPeriod = int(duration*1000 + stepDelay)
    bp.stp = 0
    #print "start timer self:", self
    mf.stepTimer.Start(stepPeriod,wx.TIMER_CONTINUOUS)
    

def takeData(self):
    #print "take data self:", self
    global plot, mf
    if bp.scanning:
        if bp.stp == 0:
            #print "deleting data"
            del bp.data[:]
            del bp.imgData[:]
        
        travel = bp.stepXY * bp.stp
        #read data value
        datVal = bp.readData(bp.duration)
        dataPoint = (travel,datVal)
        #print "scanning......",bp.scan,"...", bp.scanning
        mm = 'scanning...' + str(bp.scan)
        mf.statusbar.SetStatusText(mm, 1)
        
        #-----XY scan-------------------------------------
        #if X and Y are mdifferent, we need to "padd" the shorter axis 11apr20
        if bp.scan == "XY":
            #print "scanning XY...stp",bp.stp,"...of...X",bp.nstepX,"...Y",bp.nstepY
            ystp = bp.stp - bp.nstepX - 1

            if bp.stp == 0:
                bp.XYdata = [[datVal]]
                bp.data.append(dataPoint)
                bp.s.write(bp.gcodeTravelX)
                bp.stp += 1
                pass
                
            if bp.stp < bp.nstepX :
                #print "Scanning X of XYScan: ", bp.stp," of..",bp.nstepX
                bp.data.append(dataPoint)
                bp.XYdata.append([datVal])
                bp.s.write(bp.gcodeTravelX)
                bp.stp += 1
                pass
                
            elif bp.stp == bp.nstepX :
                #print "last X datapoint stp: ", bp.stp, "data: ", dataPoint
                bp.data.append(dataPoint)
                bp.XYdata.append([datVal])
                gcodeTravelNext = "G01 F1000 X-" + str(bp.travelCenterX) +\
                                    " Y-" + str(bp.travelCenterY) + '\r \n'
                bp.s.write(str(gcodeTravelNext))
                time.sleep( ( ( bp. travelCenterX + bp.travelCenterY) * 0.9) / bp.travelSpeed + 1)
                gcodeTravelStart = "G01 F" + str(bp.travelSpeed) + " X1 Y1" + '\r \n'
                bp.s.write(str(gcodeTravelStart))
                time.sleep(((2) * 1.2)/bp.travelSpeed + 1)
                bp.stp += 1
                    
            elif ystp == 0:
                #print "Starting Yscan of XY  ...stp: ", bp.stp, "data: ", dataPoint
                del bp.data[:]
                dataPoint = ( ystp * bp.stepXY, datVal )
                bp.data.append(dataPoint)
                bp.XYdata[ystp].append(datVal)
                bp.s.write(str(bp.gcodeTravelY))
                bp.stp += 1

            elif ystp > 0 and ystp < bp.nstepY:
                dataPoint = ( ystp * bp.stepXY, datVal )
                bp.data.append(dataPoint)
                
                if ystp > bp.nstepX:
                    bp.XYdata.append([0])
                    bp.XYdata[ystp].append(datVal)
                    pass
                else:
                    bp.XYdata[ystp].append(datVal)
                    
                bp.s.write(str(bp.gcodeTravelY))
                bp.stp += 1
                
            elif ystp == bp.nstepY :
                #print "last Y data point....."
                dataPoint = ( ystp * bp.stepXY, datVal )
                bp.data.append(dataPoint)
                if ystp > bp.nstepX:
                    bp.XYdata.append([0])
                    bp.XYdata[ystp].append(datVal)
                    pass
                else:
                    bp.XYdata[ystp].append(datVal)
                #bp.s.write(str(bp.gcodeTravelY))
                bp.scanning = False

            mm = mm + ' step  ' + str(bp.stp) + ' of ' + str(bp.nsteptot)
            mf.updateStat(mm, 1)
            
            graph()

            if bp.BPZ:
                mf.dataAnalysis
                
            pass



        #---X-scan------------------------------------
        elif bp.scan == "X":
            #print "scanning X ...stp: ", bp.stp, "  out of nsteptot: ", bp.nsteptot
            bp.data.append(dataPoint)
            bp.imgData.append(datVal)
            if bp.stp == bp.nsteptot:
                bp.scanning = False
                #print "completed X-scan..", bp.scanning
            else:    
                bp.s.write(str(bp.gcodeTravelX))
                bp.stp += 1
            mm = mm + ' step  ' + str(bp.stp) + ' of ' + str(bp.nsteptot)
            mf.updateStat(mm, 1)
            
            graph()
            pass


        #---Y-scan------------------------------------
        elif bp.scan == "Y":
            #print "scanning Y"
            bp.imgData.append(datVal)
            bp.data.append(dataPoint)
            if bp.stp == bp.nsteptot:
                #print "completed X-scan"
                bp.scanning = False
            else:    
                bp.s.write(str(bp.gcodeTravelY))
                bp.stp += 1
            mm = mm + ' step  ' + str(bp.stp) + ' of ' + str(bp.nsteptot)
            mf.updateStat(mm, 1)
            
            graph()
            pass
        

        #---2D-scan------------------------------------
        elif bp.scan == "2D":
            #print "scanning 2D"
            if bp.stp == bp.nstepX or bp.stp > bp.nstepX:
                ystp = int(math.floor(bp.stp / bp.nstepX))
            ystp = int(math.floor(bp.stp / (bp.nstepX + 1)))
            xstp = bp.stp % (bp.nstepX + 1)
                
            if bp.stp < bp.nstepX :
                ystp = 0
                xstp = bp.stp
            if ystp < bp.nstepY + 1:
                if xstp == 0:
                    del bp.data[:]
                travelX = bp.stepXY * xstp
                dataPoint = (travelX,datVal)
                bp.data.append(dataPoint)
                
                if ystp == 0:
                    if xstp == 0:
                        bp.imgData = [[datVal]]
                    else:
                        bp.imgData.append([datVal])
                else:
                    bp.imgData[xstp].append(datVal)
                    # !!! Shouldn't that be ystp????? - FD-11jan21
                    # This is correct, the list imgData[xstp] is a "vertical trace" of the 2D scan
                    
                mm = mm + ' step  ' + str(bp.stp ) + ' of ' + str(bp.nsteptot)
                mf.updateStat(mm, 1)
                graph()

                if xstp == bp.nstepX and ystp < bp.nstepY:
                    #print "completed X-scan"
                    #goback on X to edge of X-scan
                    travelBack = travelX + 1
                    #print "travelBack: ", travelBack
                    bp.s.write(str('G01 F1000 X-' + str(travelBack) + '\r \n'))
                    time.sleep(((travelBack) * 0.9)/bp.travelSpeed + 1)
                    bp.s.write(str('G01 F' + str(bp.travelSpeed) + ' X1 \r \n'))
                    time.sleep(((1) * 1.2)/bp.travelSpeed + 0.5)
                    if ystp < bp.nstepY:
                        #print "travelNextYstp: ", bp.gcodeTravelY
                        bp.s.write(str(bp.gcodeTravelY))
                        time.sleep(0.2)
                    bp.stp += 1
                elif xstp < bp.nstepX and ystp < bp.nstepY + 1:
                    bp.s.write(str(bp.gcodeTravelX))
                    bp.stp += 1
                elif xstp == bp.nstepX and ystp == bp.nstepY:
                    #print "end of 2D scan"
                    bp.scanning = False

            else:
                bp.scanning = False

            pass #end of 2Dscan
                               

        #---XYZ-scan------------------------------------
        elif bp.scan == "XYZ":           #added started  FD-31oct20
            #print ("scanning XYZ")
            pass
            

        else:
            print ("No Scan Selected")
            pass
    
    else:
        #print "MAX stp reached", bp.stp, "of ", bp.nsteptot,"...STOP SCANNING"

        #move back to center of field
        #print "travelling to center"
        mf.statusbar.SetStatusText('END OF SCAN... Moving back to center', 1)
        
        bp.s.write("G90 G0 X0 Y0 \r \n ")
        bp.s.write('G91 F' + str(bp.travelSpeed) + ' \r \n ')
        time.sleep(1)

        #print "end of scanning....",bp.scanning
        endScan(mf)


def endScan(mf):
    #print "emdscan self:", mf
    try:
        mf.stepTimer.Stop()
        #print "......... Timer Stopped!!........"
    except:
        print ("error trying to stop timer...")
        pass
    mf.btnLabel.SetLabel('START')
    mf.pulseBtn.SetBitmapSelected(mf.pulseOff)
    mf.pulseBtn.SetValue(False)
    mm = 'end scan'
    mf.updateStat(mm, 0)
    mf.updateStat('',1)
    #print ("my data:"), bp.imgData
    graph()
    if bp.scan == "2D":
        #print "calling 2Dimage"
        viewImage = showImage()
        viewImage.Show()
    elif bp.BPZ:
        mf.dataAnalysis(event = None)
    pass



def interruptScan(self):
    global mf
    """
Function called when scan has been interrupted.
Need to stop Arduino grbl, stop stages, and come nack to original position
    """

    mm = '!!scan interupted!!'
    mf.updateStat(mm, 1)
    
    #send command to Arduino to stop (pause)
    bp.s.write("! \r \n ")
    time.sleep(0.2)
    bp.s.write("~ \r \n ")

    #move back to center of field
    #print "travelling to center"
        
    mm = 'moving back center'
    mf.updateStat(mm, 0)
    
    bp.s.write("G90 G0 X0 Y0 \r \n ")
    bp.s.write('G91 F' + str(bp.travelSpeed) + ' \r \n ')
    time.sleep(1)
    
    #stopArduino()
    #bp.stopADC(bp.analogIn)

    endScan(self)
    pass
        
    

#------Functions specific to grbl-Arduino--------------------------------
#waking-up Arduino
def startArduino(grblPort, grblbdr):
    gcode =  "$I"   #asking grbl for built info

    # Open serial port
    try:
        if bp.s is None:
            pass
            bp.s = serial.Serial(bp.grblPort,bp.grblbdr)
            bp.s.timeout = 1
        else:
            if not bp.s.isOpen():
                pass
                bp.s = serial.Serial(bp.grblPort,bp.grblbdr)
                bp.s.timeout = 1
    except NameError:
        pass
        bp.s = serial.Serial(bp.grblPort,bp.grblbdr)
        bp.s.timeout = 1

    # Wake up 
    bp.s.write("\r\n\r\n") # Hit enter a few times to wake the Printrbot
    #print "waking up arduino"
    time.sleep(1)   # Wait for Arduino to initialize
    bp.s.flushInput()  # Flush startup text in serial input
    #sending string l to grbl
    bp.s.write(gcode + '\n') # Send g-code block
    #print "sent: ",gcode
    nb = bp.s.inWaiting()
    #print "Nb Bytes: ",nb
    #print "receiving response from arduino..",
    grbl_out = bp.s.readline() # Wait for response - read only first line
    #print ' : ' + grbl_out.strip()
    #setting travel speed and relative coordinates
    bp.s.write('G91 F' + str(bp.travelSpeed) + ' \r \n ')
    bp.s.write('G01 X-1 \r \n')
    bp.s.write('G01 X1 \r \n')
    time.sleep(0.5)
 
#sending G-code to Arduino
def sendCode(gcode):
    bp.s.write(str(gcode))
    bp.s.write('\r \n')
    time.sleep(0.05)

# closing serial port "s" to Arduino
def stopArduino():
    try:
        if bp.s.isOpen():
            bp.s.close()
            #print "grblComPort is closed."
        else:
            print ("s is already clsoed")
    except:
        print ("error trying to close s")
    
    
    
#------End of grbl-Arduino--------------------------------




#helper function -------
def scale_bitmap(bitmap, width, height):
    image = wx.ImageFromBitmap(bitmap)
    image = image.Scale(width, height, wx.IMAGE_QUALITY_HIGH)
    result = wx.BitmapFromImage(image)
    return result


def saveData(frame,header):
    #global data, dataInput, imgData, totalTravel, duration, travelStep, centroid, secMoment
    global mf
    
    if not bp.dataSaved:
        bp.resetProcess

    totalTravel = bp.dataInput[2]
    travelStep = bp.stepXY
    nstepX = bp.nstepX
    nstepY = bp.nstepY
    travelX = bp.travelX
    travelY = bp.travelY
    travelZ = bp.travelZ
    scan = bp.scan
    sn = bp.sn
    duration = bp.duration
    imgData = bp.imgData
    data = bp.data
    bkg = bp.bkg
    BPZ = bp.BPZ
    centroid = bp.centroid
    secMoment = bp.secMoment
    nsteptot = bp.nsteptot
    #print "saving.....duration: ", duration,"str..: ", str(duration),\
          #"..scan..",scan

    if scan == "X" or scan == "Y":
        #print "...saving...scan:",scan
        writeData = data
        #print "...saving....1st data: ",writeData
        #print "...saving....2nd data: ",writeData[1]
    elif scan == 'XYZ':
        writeData = BPZ
    else:
        writeData = imgData

    if len(writeData) > 0 :
        filetypes = "CSV files (*.csv)|*.csv|Text files (*.txt)|*.txt|All files|*"
        dlg = wx.FileDialog(frame,"Choose a file", style=wx.FD_SAVE | wx.FD_OVERWRITE_PROMPT, wildcard=filetypes)
        outFile = None
        if dlg.ShowModal()==wx.ID_OK:
            try:
                filename=dlg.GetFilename()
                dirname=dlg.GetDirectory()
                fullPath = os.path.join(dirname, filename)
                date = '"' + time.asctime() + '"'
                title = "saving data"
                bp.saveFile = filename
                #SetTitle( title + " - " + filename )

                dlg2 = wx.TextEntryDialog(frame, 'Enter some text','Text Entry')
                dlg2.SetValue("")
                if dlg2.ShowModal() == wx.ID_OK:
                    comment = dlg2.GetValue()
                    print('You entered: %s\n' % comment)
                dlg2.Destroy()

                outFile = open(fullPath, "w")
                
                # write header info
                outFile.write( date )
                outFile.write( "\n" )

                outFile.write( '"' + header + '"' )
                outFile.write( "\n" )

                outFile.write( '"' + comment + '"' )
                outFile.write( "\n" )

                outFile.write( "Scan: ," + scan )
                outFile.write( ",Nb: ," + str(sn) )
                outFile.write( "\n" )

                #should insert branch for XYZ scan  FD-31oct20
                
                outFile.write( "Step:," + str(travelStep) )
                outFile.write( ",Duration:," + str(duration) )
                outFile.write( "\n" )

                outFile.write( "Zposition: ," + str(travelZ) )
                outFile.write( ",X0: ," + str(travelX) )
                outFile.write( ",Y0: ," + str(travelY) )
                outFile.write("\n")

                outFile.write( "XYcenters: ," + str(centroid[0]) )
                outFile.write( "," + str(centroid[1]) )
                outFile.write("\n")

                outFile.write( "XY FW4S: ," + str(secMoment[0]) )
                outFile.write( "," + str(secMoment[1]) )
                outFile.write("\n")


                # write data - define nb of columns in imgData
                # depends on scan
                nData = len(writeData)
                #print ("***saving len imgData.....:"), nData,"data..",writeData[nData-1]
                
                if scan == "X" or scan == "Y":
                    cols = 2
                elif scan == "2D":
                    #print "saving 2D ncols:", bp.nstepX
                    cols = bp.nstepX
                else:
                    cols = 3

                #print "***saving....scan..",scan,"....ncols..", cols
                
                for n in range(cols):
                    #print "saving data...list row index n: ", n
                    if n == 0 and scan != "2D":
                        strfmt = str('mm,')
                        
                    elif n == 1 and scan == "X":
                        strfmt = str('X,')
                        
                    elif n == 1 and scan == "XY":
                        strfmt = str('X,')

                    elif n == 1 and scan == "Y":
                        strfmt = str('Y,')
                        
                    elif n == 2 and scan == "XY":
                        #print "col :",n,"   scan : ", scan
                        strfmt = str('Y,')
                        
                    else:
                        strfmt = ""

                    #print "saving col :", n
                    #print "initial string to be saved :", strfmt
                        
                    for nn in range(nData):
                        try:
                            if nn == nData - 1:
                                if scan == "2D":
                                    strfmt = strfmt + str(writeData[nn][n]) + str('\n')
                                elif scan == "X" or scan == "Y":
                                    strfmt = strfmt + str(writeData[nn][n]) + "\n"
                                elif n == 0:
                                    strfmt = strfmt + str(nn * travelStep) + "\n"
                                else:
                                    strfmt = strfmt + str(writeData[nn][n - 1]) + "\n"

                            else:
                                if scan == "2D":
                                    strfmt = strfmt + str(writeData[nn][n]) + str(',')
                                elif scan == "X" or scan == "Y":
                                    strfmt = strfmt + str(writeData[nn][n]) + ","
                                    #print ('..saving..'), strfmt
                                elif n == 0:
                                    strfmt = strfmt + str(nn * travelStep) + ","
                                else:
                                    strfmt = strfmt + str(writeData[nn][n - 1]) + ","
                                    
                        except IndexError:
                            print ("index error - ignored.."),nn,"//",n
                            pass
                    outFile.write(strfmt)
                outFile.close()
                print ("finished writing data in file")

                
            except IOError as e:
                mf.updateStat('IOError', 0)
                print ("Error opening file"), e
            except ValueError as e:
                mf.updateStat('ValueError', 0)
                print ("Error writing file"), e
            except:
                mf.updateStat('Error saving', 0)
                print ("Error in saving data")
                outFile.close()

            mf.updateStat('saving done', 0)
            time.sleep(1)
            bp.dataSaved = True
            mf.updateStat('', 1)
            graph()
        dlg.Destroy()

def saveBP(frame,header):
    Zpos = bp.travelZ
    Xcenter = bp.centroid[0] + bp.travelX - (0.5 * bp.Xtravel)
    Ycenter = bp.centroid[1] + bp.travelY - (0.5 * bp.Ytravel)
    Xwidth = bp.secMoment[0]
    Ywidth = bp.secMoment[1]
    writeData = [Zpos, Xcenter,Ycenter,Xwidth,Ywidth]
    filename = 'BP-' + bp.today + '.csv'
    print ("you chose to save BP only...."), Zpos,Xcenter,Ycenter,Xwidth, Ywidth
    print filename

    if writeData != [0,0,0,0,0]:
        filetypes = "CSV files (*.csv)|*.csv|Text files (*.txt)|*.txt|All files|*"
        dlg = wx.FileDialog(frame,"Choose a directory", defaultFile = filename ,style=wx.FD_SAVE | wx.FD_CHANGE_DIR, wildcard=filetypes)
        if dlg.ShowModal() == wx.ID_OK :
            dirname=dlg.GetDirectory()
            fullPath = os.path.join(dirname, filename)
            print(dirname,"....",filename,"...",fullPath)

            try:
                with open(fullPath) as f:
                    pass
            except IOError:
                # Create text input
                dlg2 = wx.TextEntryDialog(frame, 'Enter some text','Text Entry')
                dlg2.SetValue("")
                if dlg2.ShowModal() == wx.ID_OK:
                    comment = dlg2.GetValue()
                    print('You entered: %s\n' % comment)
                dlg2.Destroy()
                   
                outFile = open(fullPath, "w+")
                outFile.write( header  )
                outFile.write( "\n" )
                outFile.write( comment  )
                outFile.write( "\n" )
                outFile.write( 'Z,X0,Y0,WX,WY'  )
                outFile.write( "\n" )
                outFile.close()
                    
            with open(fullPath,'a') as f:
                print ('opening...',f)
                for n in range(len(writeData)):
                    print ('write data '), n, 'ddd ', writeData[n]
                    f.write(str(writeData[n]))
                    f.write(',')
                f.write('\n')
        dlg.Destroy()
        pass
    
    else:
        print ('no data to save')

    pass



def loadBP(event):    #  this works  FD-12nov20
    """"""
    print ("selected Load BP File...")
    global mf, rd, gd
    rd = []
    filetypes = "CSV files (*.csv)|*.csv|Text files (*.txt)|*.txt|All files|*"
    dlg = wx.FileDialog(mf,"Choose a file", style=wx.FD_OPEN | wx.FD_FILE_MUST_EXIST , wildcard=filetypes)
    if dlg.ShowModal() == wx.ID_OK :
        filename=dlg.GetFilename()
        dirname=dlg.GetDirectory()
        fullPath = os.path.join(dirname, filename)
        print(dirname,"....",filename,"...",fullPath)

        try:
            with open(fullPath) as f:
                readCSV = csv.reader(f, delimiter=',')
                for row in readCSV:
                    rd.append(row)
            pass
            
        except IOError:
            print ("error trying to read")
            pass
        print ("completed reading")
            
    try:
        gd.updateGraph()
    except NameError:
        print ('opening new graph window')
        bp.BPZdata = rd
        mf.graphBP(event=None)
            

def loadScan(event):    #  added  FD-20dec20 works for Xscan 24dec20
    """"""
    print ("selected Load scan File...")
    global mf, rd, gd
    if bp.data != []:
        loadOption = SelectOption(mf)
        loadOption.Show()
    else:
        readErase()

def readErase():
    global NewLine, AddLine, readOption
    NewLine = True
    AddLine = False
    readOption = 'erase'
    loadData()

def readWith():
    global AddLine, NewLine, readOption
    NewLine = False
    AddLine = True
    readOption = 'keep'
    loadData()

def loadData():
    global mf,rd, lineLoaded, NewLine, AddLine, readOption, rdTravel, rdImax
    #may need to reset process & data - otherwise works   FD-28dec20
    rd, rdScan, rdNb, rdStp, rdZpos, rdCenter, rdWidth, rdTravel, rdImax = readScanData()
    bp.saveFile = bp.loadedFile
    print ('rdNb:')
    print rdNb
    print ('rdData:')
    print rdCenter,'  ',rdWidth
    print ('read option'),readOption,AddLine,NewLine
    
    if rdScan == '2D':
        bp.Loaded2D = True
        rr=[]
        bp.imgData = []
        #this should have same code structure as readData for 2D scan

        """
                if ystp == 0:
                    if xstp == 0:
                        bp.imgData = [[datVal]]
                    else:
                        bp.imgData.append([datVal])
                else:
                    bp.imgData[xstp].append(datVal)
        """
        for ri in range(len(rd)):
            #rr=[]
            for ci in range(len(rd[0])):
                if ci==0 and ri==0:
                    bp.imgData = [ [ int(rd[ri][ci]) ] ]
                elif ri==0:
                    bp.imgData.append( [int(rd[ri][ci])] )
                elif ri > 0:
                    bp.imgData[ci].append( int(rd[ri][ci]) )

        bp.Ytravel = ( len(rd) - 1 ) * bp.stepXY
        bp.Xtravel = ( len(rd[0]) - 1 ) * bp.stepXY
        #mf.updateStat('',1) 
        bp.dataSaved = True         #re-instated  28jan22-FD
        viewImage = showImage()
        viewImage.Show()
        return

    if readOption == 'erase':
        print ('readOption:'),readOption
        bp.scan = rdScan
        bp.sn = int(rdNb)          #re-instated  28jan22-FD
        bp.travelZ = rdZpos
        bp.centroid = rdCenter
        bp.secMoment = rdWidth
        bp.stepXY = float(rdStp)
        bp.dataSaved = True
        bp.saveFile = bp.loadedFile
        if bp.scan == 'X' or bp.scan == 'Y':
            bp.data = dp.dataFromFile(rd,rdScan)
            if bp.scan == 'X':
                bp.Xtravel = float( bp.data[len(bp.data) - 1][0] )
            if bp.scan == 'Y':
                bp.Ytravel = float( bp.data[len(bp.data) - 1][0] ) 
            mf.updateStat('',1)
            bp.dataSaved = True
            lineLoaded = wx.lib.plot.PolyLine(bp.data, colour='RED', width=2)
        elif rdScan == 'XY':
            bp.XYdata = []
            datax, datay = dp.dataFromFile(rd,rdScan)
            bp.data = datay
            bp.stp = 2 * (len(datay)-1)
            bp.Xtravel = float( datax[len(datax) - 1][0] )
            bp.Ytravel = float( datay[len(datay) - 1][0] ) 
            mf.updateStat('',1)
            bp.dataSaved = True
            bp.nstepX = len(datax) - 1
            bp.nstepY = len(datay) - 1
            bp.nsteptot = bp.stp - 1
            for ni in range(len(datax)):
                bp.XYdata.append( (datax[ni][1],datay[ni][1]) )
            linex = wx.lib.plot.PolyLine(datax, colour='RED', width=2)
            liney = wx.lib.plot.PolyLine(datay, colour='BLUE', width=2)
            lineLoaded = [linex] + [liney]
            print ('read data - go to graph'),bp.scan
        graph()
        displayResults = DisplayResults(mf)
        displayResults.Show()

            
    if readOption == 'keep':
        print ('load & keep'), AddLine
        if rdScan == 'X' or rdScan == 'Y':
            rddata = dp.dataFromFile(rd,rdScan)
            if rdScan == bp.scan:
                lineLoaded = wx.lib.plot.PolyLine(rddata, colour='GREY', width=2)
            else:
                pass
        elif rdScan == 'XY':
            rdimgData = []
            datax, datay = dp.dataFromFile(rd,rdScan)
            for ni in range(len(datax)):
                rdimgData.append( (datax[ni][1],datay[ni][1]) )
            linex = wx.lib.plot.PolyLine(datax, colour='ORANGE', width=2)
            liney = wx.lib.plot.PolyLine(datay, colour='CYAN', width=2)
            lineLoaded = [linex] + [liney]
            print ('read data - go to graph'),rdScan
        mf.updateStat('',1)
        graph()
        displayResults = DisplayResults(mf)
        displayResults.Show()
    pass

def readScanData():
    global mf,rd
    rd = []
    rdCenter = [0,0]
    rdWidth = [0,0]
    filetypes = "CSV files (*.csv)|*.csv|Text files (*.txt)|*.txt|All files|*"
    dlg = wx.FileDialog(mf,"Choose a file", style=wx.FD_OPEN | wx.FD_FILE_MUST_EXIST , wildcard=filetypes)
    if dlg.ShowModal() == wx.ID_OK :
        filename=dlg.GetFilename()
        dirname=dlg.GetDirectory()
        fullPath = os.path.join(dirname, filename)
        bp.loadedFile = filename
        print(dirname,"....",filename,"...",fullPath)
        try:
            with open(fullPath) as f:
                print ("opening file")
                readCSV = csv.reader(f, delimiter=',')
                print ("read data successfull")
                ri = 0
                ri0 = 100
                for row in readCSV:
                    #print ('reading row'),ri," ri0 ",ri0
                    if ri == ri0 + 1:
                        rdStp = float(row[1])
                    if ri == ri0 + 2:
                        rdZpos = float(row[1])
                        try:
                            bp.rdtravelX = float(row[3])
                            bp.rdtravelY = float(row[5])
                        except:
                            pass
                    if ri == ri0 + 3:
                        rdCenter[0] = float(row[1])
                        rdCenter[1] = float(row[2])
                    if ri == ri0 + 4:
                        rdWidth[0] = float(row[1])
                        rdWidth[1] = float(row[2])
                    if ri == ri0 + 5 or ri > ri0 + 5:
                        rd.append(row)
                    if row[0] == 'Scan: ':
                        #print ('type of scan: '),row[1]
                        rdScan = row[1]
                        rdNb = int(row[3])
                        ri0 = ri
                        #print ('row ri0')
                    #print row
                    ri += 1
            pass
        except IOError:
            print ("error trying to read")
            pass
        print ("completed reading ")
        print rdScan, rdNb, rdStp
        if rdScan == 'X' or rdScan == 'Y' or rdScan == 'XY':
            rdTravel = float( rd[0][len(rd[0])-1] )
            bp.rdXtravel = rdTravel
            bp.rdYtravel = rdTravel
            rdx=[]
            rdy=[]
            if rdScan == 'XY':
                for n in range(len(rd[0]) - 1):
                    rdx.append(float(rd[1][n+1]))
                    rdy.append(float(rd[2][n+1]))
                xImax = max(rdx)
                yImax = max(rdy)
                rdImax = max(xImax,yImax)
                print ('maxI'),xImax, yImax
            else:
                for n in range(len(rd[0]) - 1):
                    rdx.append(float(rd[1][n+1]))
                rdImax = max(rdx)
        elif rdScan == '2D':
            rdTravel = ( max( len(rd) , len(rd[0]) ) - 1 ) * rdStp
            bp.rdXtravel = ( len(rd[0]) - 1 ) * rdStp
            bp.rdYtravel = ( len(rd) - 1 ) * rdStp
            Imax = max(rd)
            rdImax = float(max(Imax))
            pass
    rdImax = 1.1 * rdImax
    print ('max vals'), rdTravel, rdImax
    bp.rd = rd
    bp.rdScan = rdScan
    bp.rdNb = rdNb
    bp.rdStp = rdStp
    bp.rdZpos = rdZpos
    bp.rdCenter = rdCenter
    bp.rdWidth = rdWidth
    bp.rdTravel = rdTravel
    bp.rdImax = rdImax
    bp.LoadedFile = True
    return rd, rdScan, rdNb, rdStp, rdZpos, rdCenter, rdWidth, rdTravel, rdImax
            
    #try:
        #gd.updateGraph()
    #except NameError:
        #print ('opening new graph window')
        #bp.BPZdata = rd
        #mf.graphBP(event=None)
            
    #gd = GraphBPData(self)
    #gd.Show()






################# Starts Main Here #######################    
app = wx.App()
frame = MyFrame(None, title, xSize, ySize, panelWidth)
frame.Show()
app.MainLoop()




