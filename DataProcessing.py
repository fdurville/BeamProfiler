#! Python 3

import math
import BeamProfiler as bp

v = 3.14



#This file defines the fuctions to process data
#crated F. Durville 3 Feb 2020
#last update - FD-26oct20

#calculating default bkg value
def CalcBkg(scan,imgData):
    bkg = 0
    sumX = 0
    sumY = 0

    if scan == "2D":
        pass
    
    elif scan == "X" or scan == "Y" or scan == "XY":
        np = len(imgData)
        for nn in range(2):
            sumX = sumX + imgData[nn][1]
            sumY = sumY + imgData[nn][1]
        for nn in range(np-2,np):
            try:
                sumX = sumX + imgData[nn][1]
                sumY = sumY + imgData[nn][1]
            except IndexError:
                print ("CalcBkg index error - ignored.."),nn
                pass

    else: 
        print ("Error in CalcBkg....")
        pass
            
    bkg = sumX + sumY
    bkg = bkg / 8
    return bkg


#calculating centroid
def calc_centroid(imgData):
    Xcentroid = 0
    Ycentroid = 0
    centr = 0
    sumI = 0
    sumIx = 0
    sumIy = 0
    centroid = [0,0]
    step = bp.stepXY
    scan = bp.scan
    np = len(imgData)

    print ("calculating centroid")
    print imgData

    if scan == "X" or scan == "Y":
        for nn in range(np):
            travel = step * nn
            sumI = sumI + imgData[nn][1]
            centr = centr + travel*imgData[nn][1]
        if scan == "X":
            centroid[0] = centr / sumI
        if scan == "Y":
            centroid[1] = centr / sumI
        pass

    if scan == "XY":
        for nn in range(1,np):
            travel = step * nn
            sumIx = sumIx + imgData[nn][0]
            Xcentroid = Xcentroid + travel*imgData[nn][0]
            try:
                sumIy = sumIy + imgData[nn][1]
                Ycentroid = Ycentroid + travel*imgData[nn][1]
            except IndexError:
                print ("centroid index error - ignored.."),nn
                pass
        centroid[0] = Xcentroid / sumIx
        centroid[1] = Ycentroid / sumIy
        
    if scan == '2D':  # completed but not verififed - FD-12dec20
        npx = len(imgData[0])
        print ("length: "),np," npx ",npx
        
        for nx in range(npx):
            travel = step * nx
            Ix = 0
            for ny in range(np):
                sumI = sumI + imgData[ny][nx]
                Ix = Ix + imgData[ny][nx]
            Xcentroid = Xcentroid + travel*Ix
            
        for ny in range(np):
            travel = step * ny
            Iy = 0
            for nx in range(npx):
                Iy = Iy + imgData[ny][nx]
            Ycentroid = Ycentroid + travel*Iy
            
        centroid[0] = Xcentroid / sumI
        centroid[1] = Ycentroid / sumI
        pass
    bp.centroid = centroid
    print ("centroid:"),centroid
    return centroid

#calculating Second Moment
def calc_secmom(imgData):
    if bp.centroid[0]==0 or bp.centroid[1]==0:
        bp.centroid = calc_centroid(imgData)
        pass
    Xmom = 0
    Ymom = 0
    sumIx = 0
    sumIy = 0
    sumI = 0
    Xc = bp.centroid[0]
    Yc = bp.centroid[1]
    secMoment = [0,0]
    step = bp.stepXY
    scan = bp.scan
    np = len(imgData)
    
    if scan == "X" or scan == "Y":
        for nn in range(np):
            travel = step * nn
            if scan == "X":
                 sumIx = sumIx + imgData[nn][1]
                 Xmom = Xmom + (((travel - Xc)*(travel - Xc))*imgData[nn][1])
            if scan == "Y":
                 sumIy = sumIy + imgData[nn][1]
                 Ymom = Ymom + (((travel - Yc)*(travel - Yc))*imgData[nn][1])

        if scan == "X":     
            secMoment[0] = math.sqrt((Xmom / sumIx))
            secMoment[0] = secMoment[0] * 4
        if scan == "Y":
            secMoment[1] = math.sqrt((Ymom / sumIy))
            secMoment[1] = secMoment[1] * 4
        pass

    if scan == "XY" or scan == "XYZ":  #added FD-30oct20
        #print ("length: "),np,"imgData"
        #print imgData
        for nn in range(np):
            travel = step * nn
            sumIx = sumIx + imgData[nn][0]
            Xmom = Xmom + (((travel - Xc)*(travel - Xc))*imgData[nn][0])
            try:
                sumIy = sumIy + imgData[nn][1]
                Ymom = Ymom + (((travel - Yc)*(travel - Yc))*imgData[nn][1])
            except IndexError:
                print ("secmom index error - ignored.."),nn
                pass
        #print ("secmom sumIx sumIy Xmom Ymom..."),nn
        #print sumIx,"...",sumIy,"...",Xmom,"...",Ymom
        secMoment[0] = math.sqrt((Xmom / sumIx))
        secMoment[1] = math.sqrt((Ymom / sumIy))
        secMoment[0] = secMoment[0] * 4
        secMoment[1] = secMoment[1] * 4
        
    if scan == '2D':        # completed but not verififed  -  FD-12dec20
        npx = len(imgData[0])
        print ("length: "),np," npx ",npx
        
        for nx in range(npx):
            travel = step * nx
            Ix = 0
            for ny in range(np):
                sumI = sumI + imgData[ny][nx]
                Ix = Ix + imgData[ny][nx]
            Xmom = Xmom + (((travel - Xc)*(travel - Xc))*Ix)
            
        for ny in range(np):
            travel = step * ny
            Iy = 0
            for nx in range(npx):
                Iy = Iy + imgData[ny][nx]
            Ymom = Ymom + (((travel - Yc)*(travel - Yc))*Iy)
            
        secMoment[0] = math.sqrt((Xmom / sumI))
        secMoment[1] = math.sqrt((Ymom / sumI))
        secMoment[0] = secMoment[0] * 4
        secMoment[1] = secMoment[1] * 4
        pass
    
    print ("secMomX:"),secMoment[0],"secMomY:",secMoment[1]
    return secMoment

#reconstructing data[] to be plotted from XY imgData
def rebuildData(imgData, step):
    data = []
    np = len(imgData)
    for n in range(np):
        travel = step * n
        try:
            datVal = imgData[n][1]
            travel = step * n
            dataPoint = (travel,datVal)
            data.append(dataPoint)
        except IndexError:
            print ("rebuild index error - ignored.."),n
            pass
    return data

#reconstructing XY-data to be plotted from 2D imgData  FD-12jan21
#also rebuilds bp.imgData for further analysis - FD-16feb21
def buildXYfrom2D(imgData, step, x0, y0):
    #position of traces X-Y are defined by x0,y0
    print ("build XY from 2D")
    xo = int(x0/step)
    yo = int(y0/step)
    print xo,yo
    
    datax = []
    datay = []
    datXY = []
    npx = len(imgData)
    npy = len(imgData[0])
    np = max(npx,npy)
    print ("max data points"),np
    
    #xo = int( (npx-1)/2 )
    #yo = int( (npy-1)/2 )
    
    for n in range(npx):
        #print ("build datax")
        travel = step * n
        try:
            datValx = imgData[n][yo]
            travelx = step * n
            dataPoint = (travel,datValx)
            datax.append(dataPoint)
        except IndexError:
            print ("rebuild index error - ignored.."),n
            pass
    for n in range(npy):
        #print ("build datay")
        travel = step * n
        try:
            datValy = imgData[xo][n]
            travely = step * n
            dataPoint = (travel,datValy)
            datay.append(dataPoint)
        except IndexError:
            print ("rebuild index error - ignored.."),n
            pass

    for n in range(np):
        #print ("build datXY")
        travel = step * n
        #print travel
        try:
            datValx = imgData[n][yo]
        except IndexError:
            datValx = 0
        try:
            datValy = imgData[xo][n]
        except IndexError:
            datValy = 0
        #print ("built data point")
        dataPoint = [datValx,datValy]
        #print dataPoint
        if n == 0:
            datXY = [dataPoint]
        else:
            datXY.append(dataPoint)

    print ("complete rebuild")
    
    return datax,datay,datXY



#reconstructing data[] or datax/datay to be plotted from data file rd
def dataFromFile(rd,rdscan):
    data = []
    datax=[]
    datay=[]
    #print rd
    np = len(rd[0]) - 1
    if rdscan == 'X' or rdscan == 'Y':
        for n in range(np):
            #travel = step * n
            try:
                travel = float(rd[0][n+1])
                datVal = float(rd[1][n+1])
                if datVal * 1.1 > bp.maxY:
                    bp.maxY = 1.1 * datVal
                dataPoint = (travel,datVal)
                data.append(dataPoint)
            except IndexError:
                print ("rebuild index error - ignored.."),n
                pass
        return data
            
    elif rdscan == 'XY':
        for n in range(np):
            #travel = step * n
            try:
                travel = float(rd[0][n+1])
                datValx = float(rd[1][n+1])
                datValy = float(rd[2][n+1])
                dataPointx = (travel,datValx)
                dataPointy = (travel,datValy)
                print ('data points'),dataPointx,'  ',dataPointy
                datax.append(dataPointx)
                datay.append(dataPointy)
            except IndexError:
                print ("rebuild index error - ignored.."),n
                pass
        return datax,datay
        pass
    elif rdscan == '2D':
        npx = len(rd[0])
        npy = len(rd)
      
        pass
    #print ('')
    #print ('rebuilt data')
    #print data
    #print ('')
    #return data

#subtracting background value
def subtractBkg(scan,imgData, bkg):
    #bp.dataSaved = False
    np = len(imgData)
    nr = len(imgData[0])
    if scan == "X" or scan == "Y":
        #np = len(imgData)
        for nn in range(np):
            imgData[nn] = (imgData[nn][0],imgData[nn][1] - bkg)
            if imgData[nn][1] < 0:
                imgData[nn] = (imgData[nn][0],0)
    elif scan == "XY" or bp.xyAnalysis:        
        #np = len(imgData)
        print ("imgData length: ", np)
        for nn in range(np):
            #print "imgData value : ", imgData[nn]
            try:
                if  (imgData[nn][0] - bkg) >= 0 and  (imgData[nn][1] - bkg) >= 0:             
                    imgData[nn] = (imgData[nn][0] - bkg, imgData[nn][1] - bkg)
                elif (imgData[nn][0] - bkg) >= 0 and  (imgData[nn][1] - bkg) < 0:
                    imgData[nn] = (imgData[nn][0] - bkg, 0)
                elif (imgData[nn][0] - bkg) < 0 and  (imgData[nn][1] - bkg) >= 0:
                    imgData[nn] = (0, imgData[nn][1] - bkg)
                elif (imgData[nn][0] - bkg) < 0 and  (imgData[nn][1] - bkg) < 0: #added FD-26oct20
                    imgData[nn] = (0, 0)
            except IndexError:
                print ("subBkg index error - ignored..")
                try:
                    imgData[nn] = (imgData[nn][0],0)  #added V3-19sep20
                    if  (imgData[nn][0] - bkg) >= 0:
                        imgData[nn] = (imgData[nn][0] - bkg,0)
                    else:
                        imgData[nn] = (0,0)
                except:
                    print ("subBkg error....."),nn,"  ",imData[nn]
                pass
            
    elif scan == "2D" and not bp.xyAnalysis:
        for nn in range(np):
            for nnn in range(nr):
                try:
                    if  (imgData[nn][nr] - bkg) >= 0 :             
                        imgData[nn] = (imgData[nn][0] - bkg, imgData[nn][1] - bkg)
                    else:
                        imgData[nn] = (0, 0)
                except:
                    pass

    #print "Done sub bkg..."
    bp.resetProcess()
    return imgData

 
